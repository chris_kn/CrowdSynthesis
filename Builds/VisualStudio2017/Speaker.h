/*
==============================================================================

Speaker.h
Created: 9 Jan 2017 3:31:39pm
Author:  C. Kn�rzer

==============================================================================
*/

#ifndef SPEAKER_H_INCLUDED
#define SPEAKER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <ctime>
#include "Ringbuffer.h"

#define NOTPAUSE_PROBABILITY 0.95f

enum StreamState {
	PlaySyll = 0,
	Crossfade,
	Fadeout,
	Pausing,
	Inactive,
	NoGrainsInBuffer
};

enum Pitch {
	original = 0,
	upwards,
	downwards
};

class Speaker
	/// a speaker object represents the state of one audio string that uses grains of one voice. It contains all the parameters like the current syllable number and generates pause lengths. It implements one ring buffer that contains the next syllables that are to be played. The speaker object, however, does not know about the grain audio objects. It only stores the indexes of the grains to use.
{
public:
	Speaker()
		: position(0),
		shallSpeak(false),
		speakerState(Pausing),
		pauseLength(5000),
		newSyllable(-1),
		fadePosition(0),
		notPauseProbability(NOTPAUSE_PROBABILITY),
		grains_since_last_pause(0),
		timeUnstressed(0),
		lastGrainStressed(false)
	{
		random.setSeedRandomly();
		channels.add(0);
		levels.add(.9f);
	}
	
	~Speaker() {
		channels.clear();
		levels.clear();
		grainNumbers.reset();
	};

	// ========== PUBLIC PARAMETER =======
	bool shallSpeak;

	int position;

	int oldSyllable;
	int newSyllable;

	int fadePosition;
	int pauseLength;
	int timeUnstressed;
	bool lastGrainStressed;

	float notPauseProbability;
	int grains_since_last_pause;

	Ringbuffer grainNumbers;

	StreamState speakerState;
	Array<int> channels;

	//============ VIRTUAL METHODS ================

	virtual int getVoice() = 0;

	virtual int getPitch() = 0;

	virtual void prepareNewState(StreamState oldState, int crossFadeLength, double fs) = 0;

	//========= PUBLIC METHODS ====================

	bool isReadyToPlay()
	{
		return !grainNumbers.isEmpty();
	}

	bool isBufferFull()
	{
		return grainNumbers.isFull();
	}

	void setActive()
	{
		shallSpeak = true;
	}

	void setInactive()
	{
		shallSpeak = false;
	}

	void generatePause(int fs) {
		pauseLength = random.nextInt((int)(.05 * fs)) + (int)(.5 * fs);
	}

	void getNextSyllable() {

		oldSyllable = newSyllable;

		if (!grainNumbers.isEmpty())
		{
			newSyllable = grainNumbers.pop();
		}
		else
		{
			speakerState = NoGrainsInBuffer;
		}
	}

	void setLevel(int channel, float level)
	{
		if (level < .1f)
		{
			int index = channels.indexOf(channel);
			channels.remove(index);
			levels.remove(index);
		}
		else
		{
			channels.addIfNotAlreadyThere(channel);
			int index = channels.indexOf(channel);
			levels.resize(channels.size());
			if (index >= 0 && index < levels.size())
			{
				levels.set(index, level);
			}
		}
	}

	float getLevel(int channel)
	{
		int index = channels.indexOf(channel);
		if (index >= 0 && index < levels.size())
		{
			return levels[index];
		}
		else
		{
			return 0.f;
		}
	}

	void setPanning(Array<int> newChannels, Array<float> newLevels)
	{
		channels = newChannels;
		levels = newLevels;
	}

	void placeRandomly(int maxChannel, float level)
	{
		int newChannel = random.nextInt(maxChannel);
		channels.clear();
		levels.clear();
		channels.add(newChannel);
		levels.add(level);
	}

	bool hasChannel(int channel)
	{
		return channels.contains(channel);
	}


private:
	//=======================================================

	Random random;
	//int currentPauseProb;
	Array<float> levels;

};




#endif  // VOICE_H_INCLUDED
#pragma once
