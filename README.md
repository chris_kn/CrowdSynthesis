<h1>CrowdSynthesis PlugIn</h1>
<p>
This project was developed as part of my master thesis.
</p>

<h2>Abstract</p>
<p>
The goal of this master thesis is to implement a C++ algorithm that synthesizes the noise of a talking, human crowd and to evaluate the performance of the algorithm for different affective states within a two-dimensional valence-/arousal plane. The algorithm uses a concatenative synthesis with grains from speech recordings in an anechoic environment.
</p>
<p>
To create the grains automatically, five different syllable segmentation algorithms are implemented as MATLAB functions and compared to a manual syllable segmentation; the algorithm closest to the manual segmentation is chosen for the creation of the corpus of the concatenative synthesis.
</p>
<p>
Three different models for the handling of the selection and processing of affective states are presented: Model A1 and A2 use grains that are mapped to a position within the two-dimensional affective space. The mapping is computed manually for Model A1 and automatically with a combined regression- and classification model for Model A2. For both models, a position within the affective space and a range to determine the grains used for the concatenation can be selected. Model B selects grains during runtime according to acoustic parameters derived from the selected position within the affective space.
</p>
<p>
The output streams of the C++ algorithm of the three models implemented as VST3-plugins were convolved with binaural impulse responses and then presented to a group of 50 people who evaluated the position of the perceived affective state within the valence-/arousal plane.
</p>
<p>
Results show significant1, medium to high correlations between the (affective) position input values of the plugins and the subjects’ evaluations for both affective dimensions of Model A1 and Model A2 and for the arousal dimension of Model B. Model B fails to generate crowd noises with affective states of perceivably different valence values due to the lack of a sufficient correlation of acoustic parameters with the valence.
</p>
<p>
Several improvements and approaches for future research are suggested.
</p>

<h2>User Interface - Manual</h2>

<img src="/uploads/74939f8338990e7c40046feba8ef0bed/PlugInGUI.jpg" alt="gui"/>
<p>
The synthesizing part of Model A1 and Model A2 is identical and will thereby be referred to as Model A.
</p>
<p>
Depending on the DAW or Host used, the first thing to do after loading the Plugin is to activate all output channels, since some Hosts only activate one channel by default. For information on how to activate the output channels, look in the manual of the respective Host.
Once the output channels are activated, the audio data for the corpus has to be loaded. To do so, click on the button Load. The Model A expects a certain data structure for the audio files to be loaded: The grains for each voice have to be stored in a separate folder. After clicking on Load, select the parent folder of the folders containing the audio data. Each subfolder within this parent folder that contains audio data will be read by the plugin as one voice, that is all grains within one subfolder will be stored in one vector and used as source material for one voice. After selecting the parent folder, the Plugin scans the subfolders and reads all the audio files, applies a pitch change upwards and downwards to create three different variations (original, higher and lower) of each voice, calculates the RMSs and stores the data in buffers. This can take several minutes, depending on the corpus size. During the process, the label of the Load button shows Loading. If you use Model B, an XML-file has to be selected containing the absolute file paths and the corresponding acoustic parameters for all the audio grains. The loading process is about as long as for Model A. The window of the Plugin can be closed during the loading process, which will continue in the background. It is recommended not to interrupt the loading process by deleting the plugin during the loading process. Adding speakers or conversations is not possible before or during the loading process. (Before loading, the plugin does not know how many voices are available.)
As soon as the loading process is finished, the buttons for adding speakers and conversations can be used. The Load button now shows Load other to indicate that a different corpus can be loaded without having to restart the plugin.
</p>
<p>
In Model A, the system of coordinates on the left show the affective positions of the grains with dots in different colors. Each color represents one voice. With two sliders on the bottom and the right of the coordinates, the width and height of the selected range can be set. The selected number for the width can be seen in the slider on the bottom. The selected height is displayed in a grey font on the bottom left inside of the coordinates, on the right side of the currently selected position. The position is indicated in the coordinates by a small white square, the selected range for the available grains is shown as a grey square around the position. If no grain exists in the selected area for a voice, the speakers and conversations with (only) that voice will remain silent.
In Model B, the system of coordinates is left blank, as the grains are not assigned to a position within the system. Only a white square indicates the current position.
The rest of the user interface is identical for all models.
To add a conversation, click on the + conv button. An (empty) conversation row will appear underneath the buttons. To add a speaker to a conversation, click on a speaker icon on the top area of the conversation setup and drag and drop the icon on the middle of the conversation area. To add a single speaker without making them part of a conversation, drag the icon over the Speaker Panel on the right side of the Conversation Setup. A right rectangle indicates a drop-zone.
</p>
<p>
<img src="/uploads/84e795471b7695b11d7fdcf4f6df3f3b/Manual_Add_Speaker.jpg" alt="dnd1"/>
<img src="/uploads/b2d7ba7d756f6c61ec22e0b76debf50d/Manual_Add_Voice.jpg" alt="dnd2"/>
<img src="/uploads/4ff1d016c93fb506b3c4397275edd51b/Manual_Delete_Speaker.jpg" alt="dnd3"/>
<br>
Figure A-2: Drag-and-drop examples to add and delete speakers or conversations.
</p>
<p>
To delete a speaker, a voice of a conversation or a whole conversation, click on the respective icon and drag and drop it above the bin icon. The grey rectangles within each conversation panel serve as handles for selecting or dragging the conversation.
</p>
<p>
The +?-button on the left side of every conversation panel allow to add a random voice to the conversation. A slider on the right side, marked with a speech bubble, can be used to set the chattiness of the conversation. The default value is 0.6.
</p>
<p>
Once a conversation or speaker is selected by clicking on it (the background color changes), its routing can be adjusted via the panel labeled with out. Every rotary knob represents one output channel and sets the level, with which the audio of the selected item will be added to the channel. The button on the top of the routing panel places the selected item on a random channel. The button below places all speakers and conversations on random channels. IMPORTANT: Choosing a random placement deletes previous routing values. Each speaker or/and conversation will be placed on only one output channel.
</p>
<p>
Apart from the button to load the corpus, the general settings panel contains also two sliders: The gain knob allows the control of the main volume, the channel number selection sets the number of output channels. With some Hosts, after having adapted the number of output channels, the output channels have to be deactivated and activated again to render audio.
</p>
