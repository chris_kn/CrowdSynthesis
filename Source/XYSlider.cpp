/*
  ==============================================================================

	XYSlider.cpp
	Created: 8 Jun 2017 7:54:29pm
	Author:  C. Knörzer

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "XYSlider.h"

//==============================================================================

XYSlider::XYSlider(float xMini = 0.f, float xMaxi = 100.f, float yMini = 0.f, float yMaxi = 100.f)
	: xpos(0.f),
	ypos(0.f),
	pixSpreadX(100.f),
	pixSpreadY(100.f),
	xMin(xMini),
	xMax(xMaxi),
	yMin(yMini),
	yMax(yMaxi)
{
	pixPosX = slideValX2pix((xMax - xMin) / 2.f);
	pixPosY = slideValY2pix((yMax - yMin) / 2.f);

	addAndMakeVisible(xypoint = new XYSelector());
	xypoint->addMouseListener(this, true);
	xypoint->setMouseCursor(MouseCursor::PointingHandCursor);

	addAndMakeVisible(pos_indicator = new PositionIndicator());
}

XYSlider::~XYSlider()
{
}

void XYSlider::paint(Graphics& g)
{/// draws the zero-lines and an outline as well as the data spots once the data has been loaded

	g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
	g.setColour(Colours::grey);
	// outline
	g.drawRect(getLocalBounds(), 1);
	// cross
	g.fillRect(0, getYMiddle(), this->getWidth(), 1);
	g.fillRect(getXMiddle(), 0, 1, this->getHeight());

	// paint data spots
	if (isDataLoaded)
	{
		if (spotsX.size() == spotsY.size())
		{
			if (voiceSlider_dragged >= 0)
			{

				for (int spot = 0; spot < spotsX.size(); ++spot)
				{
					if (spotsVoice[spot] == voiceSlider_dragged)
					{
						// do nothing, as the colored spots shall be drawn on top
					}
					else
					{
						// draw grey spots
						g.setColour(Colours::darkgrey);
						g.fillEllipse(slideValX2pix(spotsX[spot]) - 1, slideValY2pix(spotsY[spot]) - 1, 3.f, 3.f);
					}
				}
				for (int spot = 0; spot < spotsX.size(); ++spot)
				{
					if (spotsVoice[spot] == voiceSlider_dragged)
					{
						g.setColour(spotcolors[spotsVoice[spot] % spotcolors.size()]);
						g.fillEllipse(slideValX2pix(spotsX[spot]) - 1, slideValY2pix(spotsY[spot]) - 1, 3.f, 3.f);
					}
					else
					{
						// do nothing, gray spots are already drawn
					}
				}
			}
			else
			{
				for (int spot = 0; spot < spotsX.size(); ++spot)
				{
					g.setColour(spotcolors[spotsVoice[spot] % spotcolors.size()]);
					g.fillEllipse(slideValX2pix(spotsX[spot]) - 1, slideValY2pix(spotsY[spot]) - 1, 3.f, 3.f);
				}
			}
		}
	}
}

void XYSlider::resized()
{/// organizes size of child components
	xypoint->setBounds(pixPosX, pixPosY, pixSpreadX, pixSpreadY);
	pos_indicator->setBounds(10, getHeight() - 25, 200, 20);
}

int XYSlider::getXMiddle()
{/// returns half of the width
	return floor(this->getWidth() / 2);
}

int XYSlider::getYMiddle()
{/// returns half of the height
	return floor(this->getHeight() / 2);
}

void XYSlider::setXSpread(float newXSpread)
{/// transforms a float slider range value in the x dimension into a pixel value and updates the slider's handle's (= the rectangle's) width 
	float center = pixPosX + pixSpreadX / 2;
	float relXspread = newXSpread / (xMax - xMin) * this->getWidth();
	pixPosX = center - relXspread / 2;
	pixSpreadX = relXspread;
	xypoint->setBounds(pixPosX, pixPosY, pixSpreadX, pixSpreadY);
}

void XYSlider::setYSpread(float newYSpread)
{/// transforms a float slider range value in the y-dimension into a pixel value and updates the slider's handle's (= the rectangle's) height
	float center = pixPosY + pixSpreadY / 2;
	float relYspread = newYSpread / (yMax - yMin) * this->getHeight();
	pixPosY = center - relYspread / 2;
	pixSpreadY = relYspread;
	pos_indicator->updateYSpread(newYSpread);
	xypoint->setBounds(pixPosX, pixPosY, pixSpreadX, pixSpreadY);
}

void XYSlider::setSliderPos(float x, float y)
{/// takes new slider position values and transforms them into pixel values and updates the handle position

	pixPosX = slideValX2pix(x) - .5f * pixSpreadX;
	pixPosY = slideValY2pix(y) - .5f * pixSpreadY;

	xypoint->setBounds(pixPosX, pixPosY, pixSpreadX, pixSpreadY);
	pos_indicator->updatePosition(x, y);
}


void XYSlider::mouseDown(const MouseEvent& event)
{/// starts a dragging event, broadcasts a change event to the plugin editor
	if (event.eventComponent == xypoint)
	{
		myDragger.startDraggingComponent(xypoint, event);
		dragCondition = 0;
		sendChangeMessage();
	}
}

void XYSlider::mouseUp(const MouseEvent& event)
{/// ends a dragging event, broadcasts a change event to the plugin editor
	if (event.eventComponent == xypoint && event.mouseWasDraggedSinceMouseDown())
	{
		dragCondition = 2;
		sendChangeMessage();
	}
}

void XYSlider::mouseDrag(const MouseEvent & event)
{/// called during a drag, updates the stored position of the handle, translates it into normed slider values and braodcasts the change to the editor
	if (event.eventComponent == xypoint && event.mouseWasDraggedSinceMouseDown())
	{
		dragCondition = 1;
		
		myDragger.dragComponent(xypoint, event, xypoint->borders);

		pixPosX = xypoint->getX();
		pixPosY = xypoint->getY();
	
		normX = (pixPosX + .5f * pixSpreadX) / (float)this->getWidth();
		normY = 1.f - (pixPosY + .5f * pixSpreadY) / (float)this->getHeight();

		xpos = pix2SlideValX(pixPosX);
		ypos = pix2SlideValY(pixPosY);

		pos_indicator->updatePosition(xpos, ypos);

		sendChangeMessage();
	}
	
}

void XYSlider::mouseWheelMove(const MouseEvent & event, const MouseWheelDetails & wheel)
{/// function could be called e.g. to change the size of the range, left for future versions
	/*if (event.eventComponent == xypoint)
	{

	}*/
}


float XYSlider::pix2SlideValX(float pixX)
{/// takes a pixel x value and calculates the corresponding slider x value
	float intermediate = xMin + (pixX + pixSpreadX / 2) * (xMax - xMin) / (float)this->getWidth();
	return (pixX + pixSpreadX / 2) * (xMax - xMin) / (float)this->getWidth() + xMin;
}

float XYSlider::pix2SlideValY(float pixY)
{/// takes a pixel y value and calculates the corresponding slider y value
	return (this->getHeight() - (pixY + pixSpreadY / 2)) / this->getHeight() * (yMax - yMin)  + yMin;
}

float XYSlider::slideValX2pix(float slideX)
{/// takes a slider x value and calculates the corresponding pixel x value
	return (slideX - xMin) * (float)this->getWidth() / (xMax - xMin);
}
float XYSlider::slideValY2pix(float slideY)
{/// takes a slider y value and calculates the corresponding pixel y value
	return ((yMax - yMin) - (slideY - yMin))* (float)this->getHeight() / (yMax - yMin);
}

