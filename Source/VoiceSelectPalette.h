/*
  ==============================================================================

    VoiceSelectPalette.h
    Created: 23 Oct 2017 4:48:21pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SpeakerIcon.h"

//==============================================================================
/*
*/
class VoiceSelectPalette    : public Component
{/// GUI component: Contains one voice icon per available voice that can be dragged on a converastion or the speaker panel for the voice to be added
public:
    VoiceSelectPalette(int numberVoices, Array<Colour> colors, int pxsize)
		: pxsize(pxsize),
		margin(2),
		numPerRow(5)
    {
		for (int i = 0; i < numberVoices; ++i)
		{
			voiceIcons.add(new SpeakerIcon(DraggedElementType::VoiceToSelectIcon, i, colors[i % colors.size()]));
			addAndMakeVisible(voiceIcons[i]);
		}

    }

    ~VoiceSelectPalette()
    {
    }

    void paint (Graphics& g) override
    {
        g.fillAll (Colours::black);   

        g.setColour (Colours::grey);
        g.drawRect (getLocalBounds(), 1);
    }

    void resized() override
    {/// organizes layout of voice symbols
        
		float w = getWidth();
		numPerRow = (int)((w - margin) / (pxsize + margin));

		if (numPerRow < 1) numPerRow = 1;

		for (int i = 0; i < voiceIcons.size(); ++i)
		{
			voiceIcons[i]->setBounds(margin + (pxsize+margin) * (i%numPerRow), margin + (pxsize+margin) * ((int)(i/numPerRow)), pxsize, pxsize);
		}
    }

	int returnHeight()
	{/// returns an estimate of the space that the icons need
		if (voiceIcons.size() % numPerRow > 0)
		{
			int test = (pxsize + margin) * ((int)(voiceIcons.size() / numPerRow) + 1) + margin;

			if (test == margin)
			{
				return 80;
			}
			else
			{
				return (pxsize + margin) * ((int)(voiceIcons.size() / numPerRow) + 1) + margin;
			}
		}
		else
		{
			int test = (pxsize + margin) * ((int)(voiceIcons.size() / numPerRow)) + margin;

			if (test == margin)
			{
				return 80;
			}
			else
			{
				return (pxsize + margin) * ((int)(voiceIcons.size() / numPerRow)) + margin;
			}
		}
	}

	OwnedArray<SpeakerIcon> voiceIcons;
	int pxsize;
	int margin;
	int numPerRow;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (VoiceSelectPalette)
};
