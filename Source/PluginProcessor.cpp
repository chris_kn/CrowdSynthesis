/*
  ==============================================================================

	This file was auto-generated!

	It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

#define MAXNUMCHANNELS 15
//#define MAXNUMVOICES 10
#define CROSSFADELENGTH 1000
#define MINIMAL_UNSTRESSED_SAMPLES 15000
#define PITCHSHIFT 1.3f
#define DEFAULTLEVEL 0.9f
#define RMSFACTOR 1.6f
#define RMS_GRADIENT 5.f
#define RMS_BASE_VALUE 0.05f
#define LASTGRAINLENGTH 0.15f

//==============================================================================
Cs_plugInAudioProcessor::Cs_plugInAudioProcessor() :
#ifndef JucePlugin_PreferredChannelConfigurations
	AudioProcessor(BusesProperties()
#if ! JucePlugin_IsMidiEffect
#if ! JucePlugin_IsSynth
		//.withInput("Input", AudioChannelSet::stereo(), true)
#endif
		.withOutput("Main Output", AudioChannelSet::mono(), true)
#endif
	),
#endif
	target_outputChannels(MAXNUMCHANNELS),
	crossfadeLength(CROSSFADELENGTH),
	minUnstressedTime(MINIMAL_UNSTRESSED_SAMPLES),
	Thread("fillGrainNumbers"),
	default_level(.9f),
	default_notPausePropability(.99f),
	//isDataLoaded(0),
	mustRefreshSelGrainMatrix(0),
	parameters(*this, nullptr),
	mustLoadNewSpeakerArray(0),
	mustLoadNewConversationArray(0),
	mustUpdateSpeaker(0),
	areThereNewPanningValues(0),
	areThereNewConvPanningValues(0),
	is_rand_voice_activated(1),
	sel_voice(0),
	sel_speaker(0),
	datastate(NotRequested),
	copybuffer1(1, 5000),
	copybuffer2(1, 5000),
	source_folder(File()),
	oscReceiver(new OSCReceiver)
{
	// Add audio parameters
	parameters.createAndAddParameter("gainParam",       // parameter ID
		"Gain",       // parameter name
		"Gain",     // parameter label (suffix)
		NormalisableRange<float>(0.0f, 1.0f),    // range
		default_level,         // default value
		nullptr,
		nullptr);

	parameters.createAndAddParameter("target_num_outputs", "Number Channels", String(), NormalisableRange<float>(1.0f, (float)target_outputChannels, 1.f), target_outputChannels, nullptr, nullptr);

	parameters.createAndAddParameter("curr_valence", "Valence", String(), NormalisableRange<float>(-100.f, 100.f, 1.f), 0.f, nullptr, nullptr);
	parameters.createAndAddParameter("curr_arousal", "Arousal", String(), NormalisableRange<float>(-100.f, 100.f, 1.f), 0.f, nullptr, nullptr);
	parameters.createAndAddParameter("curr_valence_spread", "Valence-Spread", String(), NormalisableRange<float>(10.f, 200.f, 1.f), 100.f, nullptr, nullptr);
	parameters.createAndAddParameter("curr_arousal_spread", "Arousal-Spread", String(), NormalisableRange<float>(10.f, 200.f, 1.f), 100.f, nullptr, nullptr);
	
	parameters.state = ValueTree(Identifier("CS_PlugIn_Parameters"));

	formatManager.registerBasicFormats();

	for (int numBus = 0; numBus < target_outputChannels - 1; ++numBus)
	{
		addBus(false);
	}

	parameters.addParameterListener("curr_valence", this);
	parameters.addParameterListener("curr_arousal", this);
	parameters.addParameterListener("curr_valence_spread", this);
	parameters.addParameterListener("curr_arousal_spread", this);

	updateAverageRMS(0.5f, true);

	// specify here on which UDP port number to receive incoming OSC messages
	if (!oscReceiver->connect(9001)) showConnectionErrorMessage("Error: could not connect to UDP port 9001.");

	oscReceiver->addListener(this);

}

Cs_plugInAudioProcessor::~Cs_plugInAudioProcessor()
{
	parameters.removeParameterListener("curr_valence", this);
	parameters.removeParameterListener("curr_arousal", this);
	parameters.removeParameterListener("curr_valence_spread", this);
	parameters.removeParameterListener("curr_arousal_spread", this);
	stopThread(2000);
	syllables.clear();
	speakers.clear();
	selectedGrains.clear();
	temp_selectedGrains.clear();
	//targetNumbSpeakers.clear();
}

//==============================================================================
const String Cs_plugInAudioProcessor::getName() const
{
	return JucePlugin_Name;
}

bool Cs_plugInAudioProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
	return true;
#else
	return false;
#endif
}

bool Cs_plugInAudioProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
	return true;
#else
	return false;
#endif
}

double Cs_plugInAudioProcessor::getTailLengthSeconds() const
{
	return 0.0;
}

int Cs_plugInAudioProcessor::getNumPrograms()
{
	return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
				// so this should be at least 1, even if you're not really implementing programs.
}

int Cs_plugInAudioProcessor::getCurrentProgram()
{
	return 0;
}

void Cs_plugInAudioProcessor::setCurrentProgram(int index)
{
}

const String Cs_plugInAudioProcessor::getProgramName(int index)
{
	return {};
}

void Cs_plugInAudioProcessor::changeProgramName(int index, const String& newName)
{
}

bool Cs_plugInAudioProcessor::canAddBus(bool is_input) const {

	if (is_input) return false;

	return true;
}

bool Cs_plugInAudioProcessor::canRemoveBus(bool is_input) const {
	int nbus = getBusCount(is_input);
	return (is_input ? nbus > 0 : nbus > 1);
}

//==============================================================================
void Cs_plugInAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
	/// Called right before before playback starts

	fs = sampleRate;
	//reset();
	ignoreUnused(samplesPerBlock);
	copybuffer1.setSize(1, samplesPerBlock);
	copybuffer2.setSize(1, samplesPerBlock);
	previousGain = *parameters.getRawParameterValue("gainParam");

	if (datastate == Loaded && !isThreadRunning())
	{
		startThread(5);
	}
}

void Cs_plugInAudioProcessor::releaseResources()
{
	/// Called when playback stops
	signalThreadShouldExit();
	//reset();
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Cs_plugInAudioProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{	/// Function to check if bus layout is supported. This plugin only accepts mono busses.

	if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono())
		return false;

	return true;

}
#endif

bool Cs_plugInAudioProcessor::setPreferredBusArrangement(bool isInputBus, int busIndex, const AudioChannelSet& preferredLayout)
{
	/// Function to set the bus arrangement accepted by the plugin. In this case, only mono busses are allowed
	const int numChannels = preferredLayout.size();

	// do not allow the host to disable any busses
	if (preferredLayout == AudioChannelSet::disabled())
		return false;

	// we only accept mono busses
	if (isInputBus || numChannels != 1)
		return false;

	// when accepting a layout, always fall through to the base class
	return Cs_plugInAudioProcessor::setPreferredBusArrangement(isInputBus, busIndex, preferredLayout);
}

void Cs_plugInAudioProcessor::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
	/// Function to fill output buffers. For every voice and speaker, respective syllable buffers are copied, RMS adapted if necessary and added to the output channels. This happens only on a midi or OSC request to play.
	
	// create some constants to make sure these values don't change during this runtime of the function
	const int totalNumInputChannels = getTotalNumInputChannels();
	const int totalNumOutputChannels = getTotalNumOutputChannels();

	// clear buffer
	buffer.clear();

	// update some parameters
	checkForUpdatedParams(buffer);

	// if no audio grains are loaded, we can't play anything - so we return
	if (datastate != Loaded) return;

	// MIDI PROCESSING =======

	handleMIDIEvents(midiMessages);

	// AUDIO PROCESSING =======
	
	// loop through every speaker
	for (int speakerNumber = 0; speakerNumber < speakers.size(); ++speakerNumber)
	{
		// get current speaker as constant to allow channel changes during execution
		SingleSpeaker curr_speaker = speakers[speakerNumber];
		processSpeaker(buffer, curr_speaker);
		speakers[speakerNumber] = curr_speaker;
	}

	// LOOP THROUGH CONVERSATIONS
	for (int c = 0; c < conversations.size(); ++c)
	{
		if (conversations[c].activeVoice >= 0)
		{
			NewConversation conv_copy = conversations[c];
			processConversation(buffer, conv_copy);
			conversations[c] = conv_copy;
		}
	}

	// apply current Gain (with ramp if necessary)
	const float currentGain = *parameters.getRawParameterValue("gainParam");
	
	if (currentGain == previousGain)
	{
		buffer.applyGain(currentGain);
	}
	else
	{
		buffer.applyGainRamp(0, buffer.getNumSamples(), previousGain, currentGain);
		previousGain = currentGain;
	}
}

void Cs_plugInAudioProcessor::processSpeaker(AudioSampleBuffer& buffer, SingleSpeaker & curr_speaker)
{
	processStream(buffer, curr_speaker);
}

void Cs_plugInAudioProcessor::processConversation(AudioSampleBuffer& buffer, NewConversation & conv)
{
	processStream(buffer, conv);
}

template<typename T> void Cs_plugInAudioProcessor::processStream(AudioSampleBuffer& buffer, T & curr_speaker)
{
	
	const int totalNumOutputChannels = getTotalNumOutputChannels();
	
	int outputSamplesRemaining = buffer.getNumSamples();
	int outputSamplesOffset = 0;

	// loop through all output samples to fill
	while (outputSamplesRemaining > 0)
	{
		// check speaker's state
		switch (curr_speaker.speakerState)
		{
			case StreamState::PlaySyll: // speaker is saying a syllable
			{
				// determine how many samples are copied this time
				int bufferSamplesRemaining = syllables[curr_speaker.getVoice()][curr_speaker.newSyllable].audio[curr_speaker.getPitch()].getNumSamples() - curr_speaker.position - crossfadeLength;
				int samplesThisTime = jmin(outputSamplesRemaining, bufferSamplesRemaining);

				// copy audio data from syllable matrix to copybuffer
				copybuffer1.copyFrom(0, 0, syllables[curr_speaker.getVoice()][curr_speaker.newSyllable].audio[curr_speaker.getPitch()], 0, curr_speaker.position, samplesThisTime);

				// scale audio to currently in Syllable object saved RMS factor (which was determined in previous run to avoid audible RMS jumps)
				if (curr_speaker.old_RMS_factor != 1.f)
				{
					copybuffer1.applyGain(curr_speaker.old_RMS_factor);
				}

				// get the output channels of the current speaker
				Array<int> channels = curr_speaker.channels;

				// loop through every channel of current speaker
				for (int channelAddress = 0; channelAddress < channels.size(); ++channelAddress)
				{
					if (channels[channelAddress] < totalNumOutputChannels)
					{
						// copy audio data from copybuffer to channel of output buffer
						buffer.addFrom(channels[channelAddress],
							outputSamplesOffset,
							copybuffer1,
							0,
							0,
							samplesThisTime,
							curr_speaker.getLevel(channels[channelAddress]));
					}
				}

				// subtract copied samples from remaining sample number and update speaker's position within syllable
				outputSamplesRemaining -= samplesThisTime;
				outputSamplesOffset += samplesThisTime;
				curr_speaker.position += samplesThisTime;

				// if speaker has rached end of syllable, prepare everything for the next syllable or pause
				if (curr_speaker.position == syllables[curr_speaker.getVoice()][curr_speaker.newSyllable].audio[curr_speaker.getPitch()].getNumSamples() - crossfadeLength)
				{
					curr_speaker.prepareNewState(StreamState::PlaySyll, crossfadeLength, fs);

					// if speaker speaks another syllable after the one that has just been copied, the RMS of the new syllable has to be verified and maybe adapted
					if (curr_speaker.speakerState == StreamState::Crossfade)
					{
						// compare rms values, adapt the new grain's rms to the previous one to avoid jumps
						float rms_fraction = syllables[curr_speaker.getVoice()][curr_speaker.newSyllable].rms / average_rms;

						if (rms_fraction > RMSFACTOR)
						{
							curr_speaker.new_RMS_factor = RMSFACTOR / rms_fraction;
						}
						else if (rms_fraction < 2 - RMSFACTOR)
						{
							curr_speaker.new_RMS_factor = (2 - RMSFACTOR) / rms_fraction;
						}
						else
						{
							curr_speaker.new_RMS_factor = 1.f;
						}
					}
				}

				break;
			}
			case StreamState::Crossfade: // speaker is changing from one to another syllable
			{
				int bufferSamplesRemaining = crossfadeLength - curr_speaker.fadePosition;
				int samplesThisTime = jmin(outputSamplesRemaining, bufferSamplesRemaining);

				// copy data from syllable matrix to temporate buffers for old and new syllable
				copybuffer1.copyFrom(0, 0, syllables[curr_speaker.getVoice()][curr_speaker.oldSyllable].audio[curr_speaker.getPitch()], 0, curr_speaker.position + curr_speaker.fadePosition, samplesThisTime);
				copybuffer2.copyFrom(0, 0, syllables[curr_speaker.getVoice()][curr_speaker.newSyllable].audio[curr_speaker.getPitch()], 0, curr_speaker.fadePosition, samplesThisTime);

				// create fades
				copybuffer1.applyGainRamp(0, samplesThisTime, (1 - curr_speaker.fadePosition / crossfadeLength) * curr_speaker.old_RMS_factor, (1 - (curr_speaker.fadePosition + samplesThisTime) / crossfadeLength) * curr_speaker.old_RMS_factor);
				copybuffer2.applyGainRamp(0, samplesThisTime, curr_speaker.fadePosition / crossfadeLength * curr_speaker.new_RMS_factor, (curr_speaker.fadePosition + samplesThisTime) / crossfadeLength * curr_speaker.new_RMS_factor);

				// loop through every channel
				for (int channelAddress = 0; channelAddress < curr_speaker.channels.size(); ++channelAddress)
				{
					int channel = curr_speaker.channels[channelAddress];

					if (channel < totalNumOutputChannels)
					{
						// copy data from copybuffers to output channel
						buffer.addFrom(channel,
							outputSamplesOffset,
							copybuffer2,
							0,
							0,
							samplesThisTime,
							curr_speaker.getLevel(channel));

						buffer.addFrom(channel,
							outputSamplesOffset,
							copybuffer1,
							0,
							0,
							samplesThisTime,
							curr_speaker.getLevel(channel));
					}
				}

				outputSamplesRemaining -= samplesThisTime;
				outputSamplesOffset += samplesThisTime;
				curr_speaker.fadePosition += samplesThisTime;

				// if speaker has reached the end of the crossfade, the new state (PlaySyll) has to be prepared
				if (curr_speaker.fadePosition == crossfadeLength)
				{
					curr_speaker.prepareNewState(StreamState::Crossfade, crossfadeLength, fs);
				}

				break;
			}
			case StreamState::Fadeout: // speaker will have a pause next and fades out current syllable
			{
				int bufferSamplesRemaining = crossfadeLength - curr_speaker.fadePosition;
				int samplesThisTime = jmin(outputSamplesRemaining, bufferSamplesRemaining);
			
				// copy audio data from matrix to temporate buffer and apply fade-out
				copybuffer1.copyFrom(0, 0, syllables[curr_speaker.getVoice()][curr_speaker.oldSyllable].audio[curr_speaker.getPitch()], 0, curr_speaker.position + curr_speaker.fadePosition, samplesThisTime);
				copybuffer1.applyGainRamp(0, samplesThisTime, (1 - curr_speaker.fadePosition / crossfadeLength) * curr_speaker.old_RMS_factor, (1 - (curr_speaker.fadePosition + samplesThisTime) / crossfadeLength) * curr_speaker.old_RMS_factor);

				// loop through every channel
				for (int channelAddress = 0; channelAddress < curr_speaker.channels.size(); ++channelAddress)
				{
					int channel = curr_speaker.channels[channelAddress];

					if (channel < totalNumOutputChannels)
					{
						// copy data from copybuffer to output channel
						buffer.addFrom(channel,
							outputSamplesOffset,
							copybuffer1,
							0,
							0,
							samplesThisTime,
							curr_speaker.getLevel(channel));
					}
				}

				outputSamplesRemaining -= samplesThisTime;
				outputSamplesOffset += samplesThisTime;
				curr_speaker.fadePosition += samplesThisTime;

				// if speaker reaches the end of the fade out, the pause has to be prepared
				if (curr_speaker.fadePosition == crossfadeLength)
				{
					curr_speaker.prepareNewState(StreamState::Fadeout, crossfadeLength, fs);
				}
				break;
			}
			case StreamState::Pausing: // speaker is pausing
			{
				int pauseSamplesRemaining = curr_speaker.pauseLength - curr_speaker.position;
				int samplesThisTime = jmin(outputSamplesRemaining, pauseSamplesRemaining);

				// no data is copied, position within pause is updated
				outputSamplesRemaining -= samplesThisTime;
				outputSamplesOffset += samplesThisTime;
				curr_speaker.position += samplesThisTime;

				// if speaker reaches end of pause, the new state (PlaySyll) has to be prepared
				if (curr_speaker.position == curr_speaker.pauseLength)
				{
					curr_speaker.prepareNewState(StreamState::Pausing, crossfadeLength, fs);
				}

				break;
			}
			case StreamState::NoGrainsInBuffer: // speaker is not talking due to missing available grains
			{
				outputSamplesRemaining = 0;
				outputSamplesOffset = 0;

				// check if speaker buffer contains syllable numbers; if yes, prepare new state
				if (curr_speaker.isReadyToPlay())
				{
					curr_speaker.prepareNewState(StreamState::Pausing, crossfadeLength, fs);
				}

				break;
			}
			case StreamState::Inactive: // speaker is not active, waiting to be activated by MIDI or OSC signal
			{
				outputSamplesRemaining = 0;
				outputSamplesOffset = 0;

				// check if speaker has been activated
				if (curr_speaker.shallSpeak)
				{
					curr_speaker.prepareNewState(StreamState::Pausing, crossfadeLength, fs);
				}
			}
		}
	}
}

void Cs_plugInAudioProcessor::handleMIDIEvents(MidiBuffer& midiMessages)
{/// starts and stops speakers and conversations when receiving the respective MIDI events
	const int totalNumOutputChannels = getTotalNumOutputChannels();

	MidiMessage m;
	int samplePosition;

	for (MidiBuffer::Iterator i(midiMessages); i.getNextEvent(m, samplePosition);)
	{
		if (m.isNoteOn())
		{

			int noteNumber = m.getNoteNumber();

			if (noteNumber == 24) // on C0, activate all single speakers AND conversations
			{
				for (int s = 0; s < speakers.size(); ++s)
				{
					speakers[s].setActive();
				}

				for (int c = 0; c < conversations.size(); ++c)
				{
					conversations[c].setActive();
				}
			}
			else if (noteNumber == 26) // On D0, activate only single speakers
			{
				for (int s = 0; s < speakers.size(); ++s)
				{
					speakers[s].setActive();
				}
			}
			else if (noteNumber == 28) // On E0, activate all conversations (not single speakers though)
			{
				for (int c = 0; c < conversations.size(); ++c)
				{
					conversations[c].setActive();
				}
			}
			else if (noteNumber == 29) // on F0, activate an amount of speakers depending on the velocity
			{
				float key26Velo = m.getFloatVelocity();
				
				key26Limit = key26Velo * speakers.size();

				for (int s = 0; s < key26Limit; ++s)
				{
					speakers[s].setActive();
				}
				
			}
			else if (noteNumber > 35 && noteNumber <= totalNumOutputChannels + 35 && noteNumber < 72) // on these notes, activate only one channel
			{
				int channelPressed = noteNumber - 36;

				for (int c = 0; c < totalNumOutputChannels; ++c)
				{
					
					for (int s = 0; s < speakers.size(); ++s)
					{
						if (speakers[s].channels.contains(channelPressed))
						{
							speakers[s].setActive();
						}
					}
					
					for (int con = 0; con < conversations.size(); ++con)
					{
						if (conversations[con].hasChannel(c))
						{
							conversations[con].setActive();
						}
					}
				}
			}
			else if (noteNumber > 71 && noteNumber <= conversations.size() + 71) // From C4 upwards, activate one conversation
			{
				int conv_pressed = noteNumber - 72;
				conversations[conv_pressed].setActive();
			}
		}
		else if (m.isNoteOff()) // deactivation for beforehand activated speakers
		{
			int noteNumber = m.getNoteNumber();

			if (noteNumber == 24)
			{
				
				for (int s = 0; s < speakers.size(); ++s)
				{
					speakers[s].setInactive();
				}
				
				for (int c = 0; c < conversations.size(); ++c)
				{
					conversations[c].setInactive();
				}
			}
			else if (noteNumber == 26)
			{
				
				for (int s = 0; s < speakers.size(); ++s)
				{
					speakers[s].setInactive();
				}
				
			}
			else if (noteNumber == 28)
			{
				for (int c = 0; c < conversations.size(); ++c)
				{
					conversations[c].setInactive();
				}
			}
			else if (noteNumber == 29)
			{

				int limit = jmin(key26Limit, (int)speakers.size());

				for (int s = 0; s < limit; ++s)
				{
					speakers[s].setInactive();
				}
			}
			else if (noteNumber > 35 && noteNumber <= totalNumOutputChannels + 35 && noteNumber < 72)
			{
				int channelPressed = noteNumber - 36;

				for (int c = 0; c < totalNumOutputChannels; ++c)
				{
					for (int s = 0; s < speakers.size(); ++s)
					{
						if (speakers[s].channels.contains(channelPressed))
						{
							speakers[s].setInactive();
						}
					}
					
					for (int con = 0; con < conversations.size(); ++con)
					{
						if (conversations[con].hasChannel(c))
						{
							conversations[con].setInactive();
						}
					}
				}
			}
			else if (noteNumber > 71 && noteNumber <= conversations.size() + 71)
			{
				int conv_pressed = noteNumber - 72;
				conversations[conv_pressed].setInactive();
			}
		}
	}
}

void Cs_plugInAudioProcessor::checkForUpdatedParams(AudioSampleBuffer& buffer)
{/// checks if the user or host requested a change
	// add and remove speakers
	if (mustLoadNewSpeakerArray.get() == 0 && speakers_to_add.size() > 0)
	{
		Array<int> local_to_add = speakers_to_add;

		for (int s = 0; s < local_to_add.size(); ++s)
		{
			speakers.push_back(SingleSpeaker(default_level, local_to_add[s]));
		}

		speakers_to_add.clear();
		guiUDList.add(SpeakPanel);
		sendChangeMessage();
	}
	if (mustLoadNewSpeakerArray.get() == 0 && speakers_to_remove.size() > 0)
	{
		Array<int> local_to_remove = speakers_to_remove;

		for (int s = 0; s < local_to_remove.size(); ++s)
		{
			speakers.erase(speakers.begin() + local_to_remove[s]);
		}

		speakers_to_remove.clear();
		guiUDList.add(SpeakPanel);
		sendChangeMessage();
	}
	if (mustLoadNewSpeakerArray.get() == 0 && random_speaker_to_add.size() > 0)
	{
		Array<int> local_to_add = random_speaker_to_add;

		for (int s = 0; s < local_to_add.size(); ++s)
		{
			speakers.push_back(SingleSpeaker(default_level, random.nextInt(syllables.size())));
		}
		random_speaker_to_add.clear();
		guiUDList.add(SpeakPanel);
		sendChangeMessage();
	}

	// add and remove voices to or from conversations
	if (mustLoadNewConversationArray.get() == 0 && voices_to_add.size() > 0 && add_to_this_conv.size() > 0)
	{
		Array<int> local_to_add = voices_to_add;
		Array<int> local_add_to_this = add_to_this_conv;

		for (int i = 0; i < local_to_add.size(); ++i)
		{
			conversations[local_add_to_this[i]].addVoice(local_to_add[i]);
		}

		voices_to_add.clear();
		add_to_this_conv.clear();
		guiUDList.add(ConvSetup);
		sendChangeMessage();
	}
	if (mustLoadNewConversationArray.get() == 0 && voices_to_remove.size() > 0 && remove_from_this_conv.size() > 0)
	{
		Array<int> local_to_remove = voices_to_remove;
		Array<int> local_from_this = remove_from_this_conv;

		for (int i = 0; i < local_to_remove.size(); ++i)
		{
			conversations[local_from_this[i]].removeVoice(local_to_remove[i]);
		}

		voices_to_remove.clear();
		remove_from_this_conv.clear();
		guiUDList.add(ConvSetup);
		sendChangeMessage();
	}
	if (mustLoadNewConversationArray.get() == 0 && add_random_to_conv.size() > 0)
	{
		Array<int> local_add_here = add_random_to_conv;

		for (int i = 0; i < local_add_here.size(); ++i)
		{
			conversations[local_add_here[i]].addRandomVoice(syllables.size());
		}

		add_random_to_conv.clear();
		guiUDList.add(ConvSetup);
		sendChangeMessage();
	}

	// process new panning values
	if (areThereNewPanningValues.get() == 1)
	{
		updatePanningValues(sel_speaker.get(), newPanningChannel.get(), newPanningLevel.get());
		areThereNewPanningValues.set(0);
		guiUDList.add(RoutingPanel);
		sendChangeMessage();
	}

	// check if one speaker was updated (used for random channel routing assign)
	if (mustUpdateSpeaker.get() == 1)
	{
		speakers[speakerNumbUpdatedSpeaker] = updatedSpeaker;
		mustUpdateSpeaker.set(0);
		guiUDList.add(RoutingPanel);
		sendChangeMessage();
	}

	// check if new preset or saved state was loaded or all speakers were placed randomly
	if (mustLoadNewSpeakerArray.get() == 1)
	{
		speakers = newSpeakers;
		mustLoadNewSpeakerArray.set(0);
		guiUDList.add(RoutingPanel);
		guiUDList.add(SpeakPanel);
		sendChangeMessage();
	}

	// process new conversation panning values
	if (areThereNewConvPanningValues.get() == 1)
	{
		updateConvPanningValues(sel_conv.get(), newConvPanningChannel, newConvPanningLevel);
		areThereNewConvPanningValues.set(0);
		guiUDList.add(RoutingPanel);
		sendChangeMessage();
	}

	// check if one conversation was updated
	if (mustUpdateConversation.get() == 1)
	{
		conversations[convNumbUpdatedConversation] = updatedConversation;
		mustUpdateConversation.set(0);
		guiUDList.add(ConvSetup);
		guiUDList.add(RoutingPanel);
		sendChangeMessage();
	}

	// check if new preset or saved state was loaded or all conversations were placed randomly or conversation was removed ar added
	if (mustLoadNewConversationArray.get() == 1)
	{
		conversations = newConversations;
		mustLoadNewConversationArray.set(0);
		guiUDList.add(ConvSetup);
		guiUDList.add(RoutingPanel);
		sendChangeMessage();
	}

	// adapt output channels
	const int localTargetOutputChannels = (int)*parameters.getRawParameterValue("target_num_outputs");

	if (localTargetOutputChannels > getTotalNumOutputChannels())
	{
		addBus(false);
	}
	if (localTargetOutputChannels < getTotalNumOutputChannels())
	{
		removeBus(false);
	}

	// check if background thread to fill grain numbers is still running
	if (!isThreadRunning() && (datastate == Loaded || datastate == Loading))
	{
		startThread();
	}

}

//==============================================================================
bool Cs_plugInAudioProcessor::hasEditor() const
{
	return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Cs_plugInAudioProcessor::createEditor()
{
	return new Cs_plugInAudioProcessorEditor(*this, parameters);
}

//==============================================================================
void Cs_plugInAudioProcessor::getStateInformation(MemoryBlock& destData)
{
	/// Used to save the current state of the application, the parameters ...

	// loop through voices and speakers and store panning information

	ValueTree speakerdata("SPEAKERDATA");

	for (int i = 0; i < speakers.size(); ++i)
	{
		if (speakers.size() > 0)
		{
			
			ValueTree currSpeaker("SPEAKER");
			currSpeaker.setProperty("VOICENUMBER", speakers[i].getVoice(), nullptr);
			currSpeaker.setProperty("PITCH", speakers[i].pitch, nullptr);
				
			Array<int> channels = speakers[i].channels;

			for (int k = 0; k < channels.size(); ++k)
			{
				ValueTree currChannel("CHANNEL");
				currChannel.setProperty("CHANNELNUMBER", speakers[i].channels[k], nullptr);
				float testValue = speakers[i].getLevel(channels[k]);
				currChannel.setProperty("CHANNELLEVEL", speakers[i].getLevel(channels[k]), nullptr);
				currSpeaker.addChild(currChannel, -1, nullptr);
			}

			speakerdata.addChild(currSpeaker, -1, nullptr);
		}
	}
	parameters.state.addChild(speakerdata, 0, nullptr);

	// loop through conversations to store panning information
	ValueTree conversationdata("CONVERSATIONDATA");

	for (int c = 0; c < conversations.size(); ++c)
	{
		ValueTree currConv("CONVERSATION");
		currConv.setProperty("CONVERSATIONNUMBER", c, nullptr);
		currConv.setProperty("CHATTINESS", conversations[c].chattiness.get(), nullptr);

		// get channels
		Array<int> channels = conversations[c].channels;

		for (int k = 0; k < channels.size(); ++k)
		{
			ValueTree currChannel("CHANNEL");
			currChannel.setProperty("CHANNELNUMBER", conversations[c].channels[k], nullptr);
			currChannel.setProperty("CHANNELLEVEL", conversations[c].getLevel(channels[k]), nullptr);
			currConv.addChild(currChannel, -1, nullptr);
		}

		for (int p = 0; p < conversations[c].size(); ++p)
		{
			ValueTree currVoice("VOICE");
			currVoice.setProperty("VOICENUMBER", conversations[c].voices[p], nullptr);
			currVoice.setProperty("PITCH", conversations[c].pitches[p], nullptr);
			
			currConv.addChild(currVoice, -1, nullptr);
		}

		conversationdata.addChild(currConv, -1, nullptr);
	}
	
	parameters.state.addChild(conversationdata, 0, nullptr);

	parameters.state.setProperty("sel_voice", sel_voice.get(), nullptr);
	parameters.state.setProperty("sel_speaker", sel_speaker.get(), nullptr);
	parameters.state.setProperty("sel_conv", sel_conv.get(), nullptr);
	parameters.state.setProperty("conv_sel_voice", sel_conv_voice.get(), nullptr);
	parameters.state.setProperty("is_rand_voice_activated", is_rand_voice_activated.get(), nullptr);
	parameters.state.setProperty("DATASTATE", datastate, nullptr);
	parameters.state.setProperty("TOTALNUMVOICES", (int)syllables.size(), nullptr);

	if (datastate == Loaded && source_folder.getFullPathName().isNotEmpty())
	{
		parameters.state.setProperty("SOURCE_FOLDER_PATH", source_folder.getFullPathName(), nullptr);
	}

	// Create an outer XML element..
	ScopedPointer<XmlElement> xml(parameters.state.createXml());

	if (xml != nullptr)
		copyXmlToBinary(*xml, destData);

}

void Cs_plugInAudioProcessor::setStateInformation(const void* data, int sizeInBytes)
{
	/// Function is used to load presets or saved states

	ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
	if (xmlState != nullptr)
		if (xmlState->hasTagName(parameters.state.getType()))
			parameters.state = ValueTree::fromXml(*xmlState);

	// load Data if not already loaded
	if (datastate != Loaded && (int)parameters.state.getProperty("DATASTATE") == 2 && parameters.state.hasProperty("SOURCE_FOLDER_PATH") && parameters.state.getProperty("SOURCE_FOLDER_PATH").toString().isNotEmpty() || (parameters.state.hasProperty("SOURCE_FOLDER_PATH") && parameters.state.getProperty("SOURCE_FOLDER_PATH").toString() != source_folder.getFullPathName()))
	{
		source_folder = File(parameters.state.getProperty("SOURCE_FOLDER_PATH").toString());
		datastate = Loading;
		startThread();
		sendChangeMessage();
	}

	// get single speaker data
	ValueTree speaker_data = parameters.state.getChildWithName("SPEAKERDATA");

	if (speaker_data.isValid())
	{
		std::vector<SingleSpeaker> temp_new_speakers;

		for (int i = 0; i < speaker_data.getNumChildren(); ++i)
		{

			ValueTree speaker = speaker_data.getChild(i);
			int newpitch = speaker.getProperty("PITCH");
			int curr_voice_number = speaker.getProperty("VOICENUMBER");

			if (speaker.isValid())
			{

				Array<int> newChannels;
				Array<float> newLevels;

				for (int k = 0; k < speaker.getNumChildren(); ++k)
				{
					ValueTree channel_data = speaker.getChild(k);

					if (channel_data.isValid() && channel_data.hasProperty("CHANNELNUMBER") && channel_data.hasProperty("CHANNELLEVEL"))
					{
						newChannels.add(channel_data.getProperty("CHANNELNUMBER"));
						newLevels.add(channel_data.getProperty("CHANNELLEVEL"));
					}
				}

				if (newChannels.size() > 0 && newLevels.size() > 0)
				{
					temp_new_speakers.push_back(SingleSpeaker(newChannels, newLevels, curr_voice_number, newpitch));
				}
			}
				
			
		}

		newSpeakers = temp_new_speakers;

		// get conversation data
		ValueTree conversationdata = parameters.state.getChildWithName("CONVERSATIONDATA");

		if (conversationdata.isValid())
		{
			newConversations.clear();

			for (int c = 0; c < conversationdata.getNumChildren(); ++c)
			{
				ValueTree currConv = conversationdata.getChild(c);

				if (currConv.isValid())
				{
					NewConversation newConv(0, default_level, parameters.state.getProperty("TOTALNUMVOICES"));

					Array<int> newchannels;
					Array<float> newlevels;

					for (int k = 0; k < currConv.getNumChildren(); ++k)
					{
						ValueTree currChild = currConv.getChild(k);

						if (currChild.isValid() && currChild.hasProperty("CHANNELNUMBER") && currChild.hasProperty("CHANNELLEVEL"))
						{
							newchannels.add(currChild.getProperty("CHANNELNUMBER"));
							newlevels.add(currChild.getProperty("CHANNELLEVEL"));
						}
						else if (currChild.isValid() && currChild.hasProperty("VOICENUMBER") && currChild.hasProperty("PITCH"))
						{
							int newvoice = currChild.getProperty("VOICENUMBER");
							int newpitch = currChild.getProperty("PITCH");

							newConv.addVoice(newvoice, newpitch);
						}
					}

					if (currConv.hasProperty("CHATTINESS"))
					{
						newConv.setChattiness(currConv.getProperty("CHATTINESS"));
					}

					newConv.setPanning(newchannels, newlevels);

					if (newConv.size() > 0)
					{
						newConversations.push_back(newConv);
					}
				}
			}
		}

		voices_to_add.clear();
		voices_to_remove.clear();

		sel_voice.set(parameters.state.getProperty("sel_voice"));
		sel_speaker.set(parameters.state.getProperty("sel_speaker"));
		sel_conv.set(parameters.state.getProperty("sel_conv"));
		sel_conv_voice.set(parameters.state.getProperty("conv_sel_voice"));
		is_rand_voice_activated.set(parameters.state.getProperty("is_rand_voice_activated"));

		guiUDList.add(XYSlid);

		mustLoadNewSpeakerArray.set(1);
		mustLoadNewConversationArray.set(1);
		mustRefreshSelGrainMatrix.set(1);
		sendChangeMessage();
	}

}

void Cs_plugInAudioProcessor::run()
/// background thread to fill up the ring buffers containing the indices of the next grains that are to be played
{
	
	while (!threadShouldExit())
	{
	
		if (datastate == Loading)
		{
			sendChangeMessage();
			loadDataFolders(source_folder);

			// check if thread should exit
			if (threadShouldExit()) return;

			datastate = Loaded;
			sendChangeMessage();
		}

		if (datastate == Loaded)
		{

			if (mustRefreshSelGrainMatrix.get() == 1)
			{
				buildSelectedGrainMatrix(syllables, rangeToMin(*parameters.getRawParameterValue("curr_valence"), *parameters.getRawParameterValue("curr_valence_spread")), rangeToMax(*parameters.getRawParameterValue("curr_valence"), *parameters.getRawParameterValue("curr_valence_spread")), rangeToMin(*parameters.getRawParameterValue("curr_arousal"), *parameters.getRawParameterValue("curr_arousal_spread")), rangeToMax(*parameters.getRawParameterValue("curr_arousal"), *parameters.getRawParameterValue("curr_arousal_spread")));
				mustRefreshSelGrainMatrix.set(0);
			}
			
			for (int s = 0; s < speakers.size(); ++s)
			{
				fillNextGrainBuffers(speakers[s]);
			}

			for (int c = 0; c < conversations.size(); ++c)
			{
				if (conversations[c].activeVoice >= 0)
				{
					fillNextGrainBuffers(conversations[c]);
				}
			}

			if (!isNonRealtime() && !threadShouldExit())
			{
				wait(500);
			}

		}
		//Logger::getCurrentLogger()->writeToLog("Center:_" + String(*parameters.getRawParameterValue("curr_valence")) + "_" + String(*parameters.getRawParameterValue("curr_arousal")));

	}
}

void Cs_plugInAudioProcessor::initiateLoading()
{/// starts file/folder chooser and starts side thread to load data

	File initial_directory("");
	
	// 3 lines for debug
	/*datastate = Loading;
	source_folder = File("D:\\Eigene Dateien\\Desktop\\Speech_Recordings\\Segmentation\\Grains\\Auto_Corpus_05-Aug-2017-16-30-39");
	startThread();*/

	FileChooser chooser("Choose source directory ...", initial_directory, String(), false);
	if (chooser.browseForDirectory() && chooser.getResult() != source_folder)
	{
		datastate = Loading;
		source_folder = chooser.getResult();
		startThread();
	}
}

void Cs_plugInAudioProcessor::addRandomChannel(int s)
{/// saves a random output channel and informs the audio thread to add it to the selected speaker
	
	if (s < speakers.size() && s >= 0)
	{
		updatedSpeaker = speakers[s];
		updatedSpeaker.addRandomChannel(getTotalNumOutputChannels(), default_level);
		speakerNumbUpdatedSpeaker = s;
		mustUpdateSpeaker.set(1);
	}
}

void Cs_plugInAudioProcessor::placeOneSpeakerRandomly()
{/// creates an updates speaker with a random routing channel and informs audio thread to replace the selected speaker with this one
	int s = sel_speaker.get();

	if (s > -1 && s < speakers.size())
	{
		updatedSpeaker = speakers[s];
		updatedSpeaker.placeRandomly(getTotalNumOutputChannels(), default_level);
		speakerNumbUpdatedSpeaker = s;
		mustUpdateSpeaker.set(1);
	}
}

void Cs_plugInAudioProcessor::placeAllSpeakerRandomly()
{/// creates a new speaker matrix with random panning values (every speaker gets placed on one channel only) and informs audio thread to replace the matrix.
	newSpeakers = speakers;

	for (int s = 0; s < newSpeakers.size(); ++s)
	{
		newSpeakers[s].placeRandomly(getTotalNumOutputChannels(), default_level);
	}

	mustLoadNewSpeakerArray.set(1);
}

void Cs_plugInAudioProcessor::placeConvRandomly()
{/// creates a new conversation with random routing channels and informs audio thread to replace the selcted converastion with this one.
	int c = sel_conv.get();

	if (c < conversations.size() && c >= 0)
	{
		updatedConversation = conversations[c];
		updatedConversation.placeRandomly(getTotalNumOutputChannels(), default_level);
		convNumbUpdatedConversation = c;
		mustUpdateConversation.set(1);
	}
}

void Cs_plugInAudioProcessor::placeAllConvsRandomly()
{/// creates a new conversation vector with random routing channels and tells the audio thread to replace it
	if (conversations.size() > 0)
	{
		newConversations = conversations;

		for (int c = 0; c < newConversations.size(); ++c)
		{
			newConversations[c].placeRandomly(getTotalNumOutputChannels(), default_level);
		}

		mustLoadNewConversationArray.set(1);
	}
}

void Cs_plugInAudioProcessor::loadDataFolders(File directory)
{/// goes through child folders of root folder and calls loadDataVector() to load all audio data within one folder into a seperate voice vector. If the folder doesn't contain child folders, only one vector with grains will be created. 
	
	syllables.clear();
	xValToDisp.clear();
	yValtoDisp.clear();
	voiceValtoDisp.clear();

	for (int c = 0; c < conversations.size(); ++c)
	{
		conversations[c].resetBuffer();
	}
	
	for (int s = 0; s < speakers.size(); ++s)
	{
		speakers[s].resetBuffer();
	}
	
	if (directory.containsSubDirectories())
	{
		Array<File> subdirs;

		directory.findChildFiles(subdirs, 1, false);

		for (int z = 0; z < subdirs.size(); ++z)
		{
			// check if thread should exit
			if (threadShouldExit()) return;

			std::vector<Syllable> voiceVector = loadDataVector(subdirs[z], z);
			if (!voiceVector.empty())
			{
				// add grains of voice to matrix
				syllables.push_back(voiceVector);
				
			}
		}
	}
	else
	{
		std::vector<Syllable> voiceVector = loadDataVector(directory, 0);
		if (!voiceVector.empty() && !threadShouldExit())
		{
			syllables.push_back(voiceVector);
		}
	}

	std::vector<SingleSpeaker> temp_new_speakers = speakers;
	std::vector<NewConversation> temp_new_conversations = conversations;
	
	// delete invalid voice values in conversations
	for (int c = 0; c < conversations.size(); ++c)
	{
		Array<int> todelete;

		for (int v = 0; v < conversations[c].getNumVoices(); ++v)
		{
			if (temp_new_conversations[c].getVoice(v) >= syllables.size())
			{
				todelete.add(v);
			}
		}
		
		for (int v = 0; v < todelete.size(); ++v)
		{
			temp_new_conversations[c].removeVoice(todelete[v]);
		}
	}

	// delete invalid voice values in single speakers
	Array<int> todelete;

	for (int s = 0; s < speakers.size(); ++s)
	{
		if (temp_new_speakers[s].getVoice() >= syllables.size())
		{
			todelete.add(s);
		}
	}

	for (int s = 0; s < todelete.size(); ++s)
	{
		temp_new_speakers.erase(temp_new_speakers.begin() + todelete[s]);
	}

	newSpeakers = temp_new_speakers;
	mustLoadNewSpeakerArray.set(1);
	
	newConversations = temp_new_conversations;
	mustLoadNewConversationArray.set(1);

	guiUDList.add(XYSlid);
	guiUDList.add(ConvSetup);

	// initiate selected x/y-area and fill ring buffers of grains to be played for each speaker
	buildSelectedGrainMatrix(syllables, rangeToMin(*parameters.getRawParameterValue("curr_valence"), *parameters.getRawParameterValue("curr_valence_spread")), rangeToMax(*parameters.getRawParameterValue("curr_valence"), *parameters.getRawParameterValue("curr_valence_spread")), rangeToMin(*parameters.getRawParameterValue("curr_arousal"), *parameters.getRawParameterValue("curr_arousal_spread")), rangeToMax(*parameters.getRawParameterValue("curr_arousal"), *parameters.getRawParameterValue("curr_arousal_spread")));
}

std::vector<Syllable> Cs_plugInAudioProcessor::loadDataVector(File directory, int voicenumber)
{/// scans the given directoy for all WAVE-audio files and loads them into a vector
	Array<File> audiofiles;
	std::vector<Syllable> voiceVector;
	directory.findChildFiles(audiofiles, 2, false, "*.wav");

	for (int y = 0; y < audiofiles.size(); ++y)
	{
		// check if thread should exit
		if (threadShouldExit())
		{
			return voiceVector;
		}

		// create temporary reader
		ScopedPointer<AudioFormatReader> reader(formatManager.createReaderFor(audiofiles[y]));

		if (reader != nullptr)
		{
			double duration = reader->lengthInSamples / reader->sampleRate;

			// ignore files that are longer than one second and shorter than 2 times the cross fade length
			float testValue = reader->lengthInSamples / pow(2.f, (float)PITCHSHIFT*(1.f / 12.f));

			if (duration < 0.8f && (reader->lengthInSamples / pow(2.f, (float)PITCHSHIFT*(1.f/12.f)) > (2.f * (float)crossfadeLength)))
			{

				AudioBuffer<float> newBufferFile(reader->numChannels, reader->lengthInSamples);

				// copy data from reader to buffer
				reader->read(&newBufferFile,
					0,
					reader->lengthInSamples,
					0,
					true,
					true);

				// read file name and get x, y and p value out of it
				String filename = audiofiles[y].getFileNameWithoutExtension();

				int xValStart = filename.indexOf(0, "X_") + 2;
				int yValStart = filename.indexOf(0, "Y_") + 2;
				int pValStart = filename.indexOf(0, "P_") + 2;

				if (xValStart != -1 && yValStart != -1 && pValStart != -1)
				{
					String leftover = filename.substring(xValStart, filename.length());
					int xValEnd = leftover.indexOfChar('_') - 1 + xValStart;
					leftover = filename.substring(yValStart, filename.length());
					int yValEnd = leftover.indexOfChar('_') - 1 + yValStart;
					leftover = filename.substring(pValStart, filename.length());
					int pValEnd = leftover.indexOfChar('_') - 1 + pValStart;
					if (pValEnd == -2)pValEnd = filename.length() - 1;

					String xVal_string = filename.substring(xValStart, xValEnd + 1);
					String yVal_string = filename.substring(yValStart, yValEnd + 1);
					String pVal_string = filename.substring(pValStart, pValEnd + 1);

					float xVal = xVal_string.getFloatValue();
					float yVal = yVal_string.getFloatValue();
					float pVal = pVal_string.getFloatValue();

					if (-100.f <= xVal && xVal <= 100.f && -100.f <= yVal && yVal <= 100.f)
					{
						// create syllable object
						voiceVector.push_back(Syllable(newBufferFile, reader->sampleRate, xVal, yVal, pVal, PITCHSHIFT));

						bool unique = true;
						int counter = 0;
						// check if point on position already exists
						while (counter < xValToDisp.size() && unique == true)
						{
							if (xValToDisp[counter] == xVal && yValtoDisp[counter] == yVal && voiceValtoDisp[counter] == voicenumber)
							{
								unique = false;
							}
							++counter;
						}
						if (unique == true)
						{
							// create syllable object and append to matrix
							xValToDisp.push_back(xVal);
							yValtoDisp.push_back(yVal);
							voiceValtoDisp.push_back(voicenumber);
						}
					}
					else
					{
						//Logger::getCurrentLogger()->writeToLog("x or y value for file " + filename + " not valid. Expected only values -100 - 100.");
					}
				}
				else
				{
					//Logger::getCurrentLogger()->writeToLog("X, Y, and/or P informtaion of file " + filename + " not found. File wasn't added to corpus.");
				}
			}
			else
			{
				Logger::getCurrentLogger()->writeToLog("File " + String(y) + " too long or too short.");
			}
		}
	}

	// check if thread should exit
	if (threadShouldExit()) return voiceVector;

	std::sort(voiceVector.begin(), voiceVector.end());
	return voiceVector;
}

void Cs_plugInAudioProcessor::fillNextGrainBuffers(SingleSpeaker & speaker)
{///Fills the speakers' grain buffers with numbers serving as indices os grains in the syllable matrix
	GrainBuffer & buffer = speaker.getBuffer();

	if (selectedGrains[speaker.getVoice()].size() > 4)
	{
		Array<int> temp_grainVector = selectedGrains[speaker.getVoice()];

		while ((!buffer.isFull()) && temp_grainVector.size() > 4)
		{
			int halfIndex = (int)temp_grainVector.size() / 2;
			int curr_RMS_difference = 0;

			if (buffer.timeUnstressed < minUnstressedTime)
			{
				// unstressed syllable
				int nextIndice = random.nextInt(halfIndex);
				int nextSyllable = temp_grainVector[nextIndice];

				if (buffer.tryPush(nextSyllable))
				{
					temp_grainVector.removeAndReturn(nextIndice);
					buffer.timeUnstressed += syllables[speaker.getVoice()][nextSyllable].audio[speaker.getPitch()].getNumSamples();
					buffer.lastGrainLength = syllables[speaker.getVoice()][nextSyllable].audio[speaker.getPitch()].getNumSamples();
					buffer.lastGrainStressed = false;
					++buffer.grains_since_last_pause;
					buffer.firstGrainRMS = buffer.secondGrainRMS;
					buffer.secondGrainRMS = syllables[speaker.getVoice()][nextSyllable].rms;
					curr_RMS_difference = buffer.firstGrainRMS - buffer.secondGrainRMS;
				}
			}
			else
			{
				// stressed syllable
				int nextIndice = halfIndex + random.nextInt(halfIndex - 1);
				int nextSyllable = temp_grainVector[nextIndice];

				if (buffer.tryPush(nextSyllable))
				{
					temp_grainVector.removeAndReturn(nextIndice);
					buffer.timeUnstressed = 0;
					buffer.lastGrainLength = syllables[speaker.getVoice()][nextSyllable].audio[speaker.getPitch()].getNumSamples();
					buffer.lastGrainStressed = true;
					++buffer.grains_since_last_pause;
					buffer.firstGrainRMS = buffer.secondGrainRMS;
					buffer.secondGrainRMS = syllables[speaker.getVoice()][nextSyllable].rms;
					curr_RMS_difference = buffer.firstGrainRMS - buffer.secondGrainRMS;
				}
			}
			if (buffer.lastGrainStressed == false && buffer.grains_since_last_pause > buffer.min_grains_before_pause && curr_RMS_difference > 0 && buffer.lastGrainLength / fs > LASTGRAINLENGTH)
			{
				// pause lottery
				float pauselottery = random.nextFloat();

				if (pauselottery > buffer.notPauseProbability)
				{
					if (buffer.tryPush(-1))
					{
						buffer.lastGrainLength = 0;
						buffer.notPauseProbability = default_notPausePropability;
						buffer.grains_since_last_pause = 0;
					}
				}
				else
				{
					buffer.notPauseProbability *= default_notPausePropability;
				}
			}

			// refill vector with possible numbers when only x grains are left
			if (temp_grainVector.size() < 5)
			{
				temp_grainVector = selectedGrains[speaker.getVoice()];
			}
		}
	}
}

void Cs_plugInAudioProcessor::fillNextGrainBuffers(NewConversation & conv)
{///Fills the conversations' grain buffers with numbers serving as indices os grains in the syllable matrix

	const int voicenum = conv.getVoice();
	GrainBuffer & grainbuffer = conv.getBuffer();

	if (selectedGrains[voicenum].size() > 4)
	{
		Array<int> temp_grainVector = selectedGrains[voicenum];

		while (!grainbuffer.isFull() && temp_grainVector.size() > 4)
		{
			int halfIndex = (int)temp_grainVector.size() / 2;
			int curr_RMS_difference = 0;

			if (grainbuffer.timeUnstressed < minUnstressedTime)
			{
				// unstressed syllable
				int nextIndice = random.nextInt(halfIndex);
				int nextSyllable = temp_grainVector[nextIndice];

				if (grainbuffer.tryPush(nextSyllable))
				{
					temp_grainVector.removeAndReturn(nextIndice);
					grainbuffer.timeUnstressed += syllables[voicenum][nextSyllable].audio[conv.getPitch()].getNumSamples();
					grainbuffer.lastGrainLength = syllables[voicenum][nextSyllable].audio[conv.getPitch()].getNumSamples();
					grainbuffer.lastGrainStressed = false;
					++grainbuffer.grains_since_last_pause;
					grainbuffer.firstGrainRMS = grainbuffer.secondGrainRMS;
					grainbuffer.secondGrainRMS = syllables[voicenum][nextSyllable].rms;
					curr_RMS_difference = grainbuffer.firstGrainRMS - grainbuffer.secondGrainRMS;
				}
			}
			else
			{
				// stressed syllable
				int nextIndice = halfIndex + random.nextInt(halfIndex - 1);
				int nextSyllable = temp_grainVector[nextIndice];

				if (grainbuffer.tryPush(nextSyllable))
				{
					temp_grainVector.removeAndReturn(nextIndice);
					grainbuffer.timeUnstressed = 0;
					grainbuffer.lastGrainLength = syllables[voicenum][nextSyllable].audio[conv.getPitch()].getNumSamples();
					grainbuffer.lastGrainStressed = true;
					++grainbuffer.grains_since_last_pause;
					grainbuffer.firstGrainRMS = grainbuffer.secondGrainRMS;
					grainbuffer.secondGrainRMS = syllables[voicenum][nextSyllable].rms;
					curr_RMS_difference = grainbuffer.firstGrainRMS - grainbuffer.secondGrainRMS;
				}
			}
			if (grainbuffer.lastGrainStressed == false && grainbuffer.grains_since_last_pause > grainbuffer.min_grains_before_pause && curr_RMS_difference > 0 && (float)grainbuffer.lastGrainLength / fs > LASTGRAINLENGTH)
			{
				// pause lottery
				float pauselottery = random.nextFloat();

				if (pauselottery > grainbuffer.notPauseProbability)
				{
					if (grainbuffer.tryPush(-1))
					{
						grainbuffer.notPauseProbability = default_notPausePropability;
						grainbuffer.grains_since_last_pause = 0;
						grainbuffer.lastGrainLength = 0;
					}
				}
				else
				{
					grainbuffer.notPauseProbability *= default_notPausePropability;
				}
			}

			// refill vector with possible numbers when only x grains are left
			if (temp_grainVector.size() < 5)
			{
				temp_grainVector = selectedGrains[voicenum];
			}
		}
	}
}

template<typename T> void Cs_plugInAudioProcessor::fillNextGrainBuffersTemplate(T & speaker)
{/// function that fills up ring buffers of every speaker on a background thread, currently not used
	if (selectedGrains[speaker.getVoice()].size() > 4)
	{
		Array<int> temp_grainVector = selectedGrains[speaker.getVoice()];

		while ((!speaker.isBufferFull()) && temp_grainVector.size() > 4)
		{
			int halfIndex = (int)temp_grainVector.size() / 2;
			int curr_RMS_difference = 0;

			if (speaker.timeUnstressed < minUnstressedTime)
			{
				// unstressed syllable
				int nextIndice = random.nextInt(halfIndex);
				int nextSyllable = temp_grainVector[nextIndice];

				if (speaker.grainNumbers.tryPush(nextSyllable))
				{
					temp_grainVector.removeAndReturn(nextIndice);
					speaker.timeUnstressed += syllables[speaker.getVoice()][nextSyllable].audio[speaker.getPitch()].getNumSamples();
					speaker.lastGrainStressed = false;
					++speaker.grains_since_last_pause;
					speaker.firstGrainRMS = speaker.secondGrainRMS;
					speaker.secondGrainRMS = syllables[speaker.getVoice()][nextSyllable].rms;
					curr_RMS_difference = speaker.firstGrainRMS - speaker.secondGrainRMS;
				}
			}
			else
			{
				// stressed syllable
				int nextIndice = halfIndex + random.nextInt(halfIndex - 1);
				int nextSyllable = temp_grainVector[nextIndice];

				if (speaker.grainNumbers.tryPush(nextSyllable))
				{
					temp_grainVector.removeAndReturn(nextIndice);
					speaker.timeUnstressed = 0;
					speaker.lastGrainStressed = true;
					++speaker.grains_since_last_pause;
					speaker.firstGrainRMS = speaker.secondGrainRMS;
					speaker.secondGrainRMS = syllables[speaker.getVoice()][nextSyllable].rms;
					curr_RMS_difference = speaker.firstGrainRMS - speaker.secondGrainRMS;
				}
			}
			if (speaker.lastGrainStressed == false && speaker.grains_since_last_pause > speaker.get_min_grains_before_pause() && curr_RMS_difference > 0)
			{
				// pause lottery
				float pauselottery = random.nextFloat();

				if (pauselottery > speaker.notPauseProbability)
				{
					if (speaker.grainNumbers.tryPush(-1))
					{
						speaker.notPauseProbability = default_notPausePropability;
						speaker.grains_since_last_pause = 0;
					}
				}
				else
				{
					speaker.notPauseProbability *= default_notPausePropability;
				}
			}
			
			// refill vector with possible numbers when only x grains are left
			if (temp_grainVector.size() < 5)
			{
				temp_grainVector = selectedGrains[speaker.getVoice()];
			}
		}
	}
	//selectedGrains = temp_selectedGrains;
}

void Cs_plugInAudioProcessor::buildSelectedGrainMatrix(std::vector<std::vector<Syllable>> &corpus, float xMin, float xMax, float yMin, float yMax)
{/// takes selected x/y-area and returns the grain indices of the syllable matrix that fit the selection
	std::vector<Array<int>> newMatrix;

	for (unsigned int i = 0; i < corpus.size(); ++i) {
		
		Array<int> temp;

		for (unsigned int j = 0; j < corpus[i].size(); ++j)
		{
			if ((xMin <= corpus[i][j].xPos) && (corpus[i][j].xPos <= xMax) && (yMin <= corpus[i][j].yPos) && (corpus[i][j].yPos <= yMax))
			{
				temp.add(j);
			}
		}
		
		std::sort(temp.begin(), temp.end());
		newMatrix.push_back(temp);
	}

	selectedGrains = newMatrix;
}

float Cs_plugInAudioProcessor::rangeToMin(float middle, float range)
{/// returns the minimum of a given range with center "middle"
	return middle - range * .5f;
}

float Cs_plugInAudioProcessor::rangeToMax(float middle, float range)
{/// returns the maximum of a given rangewith center "middle"
	return middle + range * .5f;
}

void Cs_plugInAudioProcessor::updatePanningValues(int speaker, int channel, float level)
{/// private function called by the audio thread to update a single speaker panning value
	if (speaker < speakers.size() && channel < getTotalNumOutputChannels())
	{
		speakers[speaker].setLevel(channel, level);
	}
}

void Cs_plugInAudioProcessor::updateConvPanningValues(int num_conv, int channel, float level)
{/// private function called by the audio thread to update a conversation's panning value
	if (num_conv < conversations.size() && channel < getTotalNumOutputChannels())
	{
		conversations[num_conv].setLevel(channel, level);
	}
}

void Cs_plugInAudioProcessor::analyseCorpus(std::vector<std::vector<Syllable>>& corpus)
{/// function to analyse the corpus, currently not used
	int num_voices = corpus.size();
	std::vector<int> mean_grain_length(num_voices);

	for (int v = 0; v < num_voices; ++v)
	{
		int sum_length = 0;

		for (int g = 0; g < corpus[v].size(); ++g)
		{
			sum_length += corpus[v][g].audio[0].getNumSamples();
		}

		mean_grain_length[v] = sum_length / corpus[v].size();
	}

}

void Cs_plugInAudioProcessor::parameterChanged(const String & parameterID, float newValue)
{/// called if one of the audio processor parameters is changed, sends the change message to the editor
	if (parameterID == "curr_valence" || parameterID == "curr_arousal" || parameterID == "curr_valence_spread" || parameterID == "curr_arousal_spread")
	{
		guiUDList.add(XYSlid);
		sendChangeMessage();
		mustRefreshSelGrainMatrix.set(1);
		if (parameterID == "curr_arousal")
		{
			updateAverageRMS(*parameters.getRawParameterValue("curr_arousal"), false);
		}
	}
}

void Cs_plugInAudioProcessor::oscMessageReceived(const OSCMessage & message)
{/// Function that handles received OSC messages
	
	String pattern = message.getAddressPattern().toString();

	if (pattern == "/cs/talk")
	{
		for (int c = 0; c < conversations.size(); ++c)
		{
			conversations[c].setActive();
		}
	}
	else if (pattern == "/cs/stop")
	{
		for (int c = 0; c < conversations.size(); ++c)
		{
			conversations[c].setInactive();
		}
	}
	else if (pattern == "/cs/conversation/add")
	{
		if (message.size() == 1 && message[0].isInt32())
		{
			addConversations(message[0].getInt32());
		}
		else if (message.isEmpty())
		{
			addConversation();
		}
	}
	else if (pattern == "/cs/conversation/delete")
	{
		if (message.isEmpty()) return;

		for (int i = 0; i < message.size(); ++i)
		{
			if (message[i].isInt32() && message[i].getInt32() >= 0 && message[i].getInt32() < conversations.size())
			{
				removeConversation(message[i].getInt32());
			}
		}
		
	}
	else if (pattern == "/cs/conversation/addVoice")
	{
		if (message.size() > 1 && message[0].isInt32())
		{
			sel_conv.set(message[0].getInt32());
		
			for (int i = 1; i < message.size(); ++i)
			{
				if (message[i].isInt32() && message[i].getInt32() >= 0 && message[i].getInt32() < syllables.size())
				{
					voices_to_add.add(message[i].getInt32());
				}
			}
		}
	}
	else if (pattern == "/cs/conversation/deleteVoice")
	{
		if (message.size() == 1 && message[0].isInt32())
		{
			sel_conv.set(message[0].getInt32());

			int last_voice = conversations[sel_conv.get()].getLastVoice();

			if (last_voice > -1)
			{
				voices_to_remove.add(last_voice);
			}
		}
		else if (message.size() > 1 && message[0].isInt32())
		{
			sel_conv.set(message[0].getInt32());

			for (int i = 1; i < message.size(); ++i)
			{
				if (message[i].isInt32() && message[i].getInt32() >= 0 && message[i].getInt32() < syllables.size())
				{
					voices_to_remove.add(message[i].getInt32());
				}
			}
		}
	}
	else if (pattern == "/cs/conversation/setChattiness")
	{
		if (message.size() == 2 && message[0].isInt32() && message[1].isFloat32())
		{
			setChattiness(message[0].getInt32(), message[1].getFloat32());
		}

		guiUDList.add(ConvSetup);
		sendChangeMessage();
	}
	else if (pattern == "/cs/gain")
	{
		if (message.size() == 1 && message[0].isFloat32())
		{
			parameters.getParameter("gainParam")->setValueNotifyingHost(message[0].getFloat32());
		}
	}
	else if (pattern == "/cs/load")
	{
		if (message.isEmpty()) return;

		if (message[0].isString())
		{
			String path = message[0].getString();
			File dir(path);
			if (dir.isDirectory())
			{
				datastate = Loading;
				source_folder = dir;
				startThread();
			}
		}
	}
	else if (pattern == "/cs/setX")
	{
		if (message.isEmpty()) return;

		if (message.size() == 1 && message[0].isFloat32() && message[0].getFloat32() <= 100.f && message[0].getFloat32() > -100.f)
		{
			float normX = (message[0].getFloat32() + 100.f) / 200.f;
			parameters.getParameter("curr_valence")->setValueNotifyingHost(normX);
		}
	}
	else if (pattern == "/cs/setY")
	{
		if (message.isEmpty()) return;

		if (message.size() == 1 && message[0].isFloat32() && message[0].getFloat32() <= 100.f && message[0].getFloat32() > -100.f)
		{
			float normY = (message[0].getFloat32() + 100.f) / 200.f;
			parameters.getParameter("curr_arousal")->setValueNotifyingHost(normY);
		}
	}
	else if (pattern == "/cs/setXSpread")
	{
		if (message.isEmpty()) return;

		if (message.size() == 1 && message[0].isFloat32() && message[0].getFloat32() <= 100.f && message[0].getFloat32() > -100.f)
		{
			float normX = (message[0].getFloat32() - 9.f) / 200.f;
			parameters.getParameter("curr_valence_spread")->setValueNotifyingHost(normX);
		}
	}
	else if (pattern == "/cs/setYSpread")
	{
		if (message.isEmpty()) return;

		if (message.size() == 1 && message[0].isFloat32() && message[0].getFloat32() <= 100.f && message[0].getFloat32() > -100.f)
		{
			float normY = (message[0].getFloat32() - 9.f) / 200.f;
			parameters.getParameter("curr_arousal_spread")->setValueNotifyingHost(normY);
		}
	}
}

void Cs_plugInAudioProcessor::addConversation()
{/// creates a new conversation vector and informs the audio thread to replace the current one with the new one
	newConversations = conversations;
	newConversations.push_back(NewConversation(0, default_level, syllables.size()));
	mustLoadNewConversationArray.set(1);
}

void Cs_plugInAudioProcessor::addConversations(int howMany)
{/// creates a new conversation vector and informs the audio thread to replace the current one with the new one
	newConversations = conversations;

	for (int i = 0; i < howMany; ++i)
	{
		newConversations.push_back(NewConversation(0, default_level, syllables.size()));
	}
	
	mustLoadNewConversationArray.set(1);
}

void Cs_plugInAudioProcessor::removeConversation(int num_conv)
{/// creates a new converastion vector without the one that was requested to remove and informs the audio thread to update the vector
	if (num_conv > -1 && num_conv < conversations.size())
	{	
		newConversations = conversations;
		newConversations.erase(newConversations.begin() + num_conv);
		mustLoadNewConversationArray.set(1);
	}

	if (newConversations.size() > 0)
	{
		sel_conv.set(newConversations.size() - 1);
	}
	else
	{
		sel_conv.set(0);
	}

}

void Cs_plugInAudioProcessor::setChattiness(int convIndex, float value)
{/// set chattiness of conversation with index "convIndex" to float "value"
	if (convIndex >= 0 && convIndex < conversations.size())
	{
		if (value < 0.f)
		{
			value = 0.f;
		}
		else if (value > 1.f)
		{
			value = 1.f;
		}

		conversations[convIndex].setChattiness(value);
	}
}

float Cs_plugInAudioProcessor::getChattiness(int convIndex)
{/// returns chattiness of conversation with requested index if conversation exists
	if (convIndex >= 0 && convIndex < conversations.size())
	{
		return conversations[convIndex].chattiness.get();
	}
	else
	{
		return .6f;
	}
}

void Cs_plugInAudioProcessor::updateAverageRMS(float arousal, bool normed)
{/// calculates the current average RMS value that grains are adapted to (with certain tolerance) to avoid jumps
	if (normed)
	{
		average_rms = arousal * RMS_GRADIENT *  RMS_BASE_VALUE + RMS_BASE_VALUE;
	}
	else
	{
		average_rms = ((arousal + 100.f) / 200.f) * RMS_GRADIENT *  RMS_BASE_VALUE + RMS_BASE_VALUE;
	}
	
}

void Cs_plugInAudioProcessor::showConnectionErrorMessage(const String& messageText)
{/// called when osc connection fails
	AlertWindow::showMessageBoxAsync(
		AlertWindow::WarningIcon,
		"Connection error",
		messageText,
		"OK");
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
	return new Cs_plugInAudioProcessor();
}
