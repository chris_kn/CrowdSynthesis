/*
  ==============================================================================

	Speaker.h
	Created: 9 Jan 2017 3:31:39pm
	Author:  C. Knörzer

  ==============================================================================
*/

#ifndef SPEAKER_H_INCLUDED
#define SPEAKER_H_INCLUDED

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
#include <ctime>
#include "Grainbuffer.h"
#include "TalkingStream.h"

class SingleSpeaker : public TalkingStream
/// Speaker object representing one speaker with a constant voice (not like with a conversation). Derives from TalkingStream.
{
public:
	SingleSpeaker()
	{
		pitch = static_cast<Pitch>(random.nextInt(3));
	};

	SingleSpeaker(float default_level, int voiceNumb)
		: TalkingStream(default_level),
		voice(voiceNumb)
	{
		pitch = static_cast<Pitch>(random.nextInt(3));
	};
	// constructor for loading speakers from saved state
	SingleSpeaker(Array<int> newchannels, Array<float> newlevels, int voiceNumb, int newpitch)
		: TalkingStream(newchannels, newlevels),
		voice(voiceNumb)
	{
		pitch = static_cast<Pitch>(newpitch);
	}

	~SingleSpeaker()
	{
		grainNumbers.reset();
	}
	// ========== Public Params =======
	int voice;
	Pitch pitch;
	GrainBuffer grainNumbers;
	//============ Public Methods ================

	
	bool isReadyToPlay()
	{/// checks if buffer is not empty
		return !grainNumbers.isEmpty();
	}

	bool isBufferFull()
	{/// check if grain ring buffer is full
		return grainNumbers.isFull();
	}

	GrainBuffer & getBuffer()
	{/// returns the reference of the buffer
		return grainNumbers;
	}

	int getVoice()
	{/// returns the voice number of the speaker
		return voice;
	}

	int getPitch()
	{/// returns the "pitch" (original, up or down) that the speaker uses
		return static_cast<int>(pitch);
	}

	void prepareNewState(StreamState oldState, int crossFadeLength, double fs)
	{/// handles switching from one state to another, that is in between syllables or after or before pauses
		switch (oldState)
		{
		case StreamState::PlaySyll:

			oldSyllable = newSyllable;
			fadePosition = 0;

			if (!grainNumbers.isEmpty() && shallSpeak)
			{
				newSyllable = grainNumbers.pop();
				speakerState = StreamState::Crossfade;
			}
			if (grainNumbers.isEmpty() || newSyllable == -1 || !shallSpeak)
			{
				speakerState = StreamState::Fadeout;
				generatePause(fs);
			}

			break;
		case StreamState::Crossfade:
			position = crossFadeLength;
			speakerState = StreamState::PlaySyll;
			old_RMS_factor = new_RMS_factor;
			break;
		case StreamState::Fadeout:
			position = 0;
			speakerState = StreamState::Pausing;
			resetRMS();
			break;
		case StreamState::Pausing:
			position = 0;
			oldSyllable = newSyllable;

			if (!grainNumbers.isEmpty() && shallSpeak)
			{
				newSyllable = grainNumbers.pop();

				if (newSyllable < 0)
				{
					speakerState = StreamState::Pausing;
					generatePause(fs);
				}
				else
				{
					speakerState = StreamState::PlaySyll;
				}
			}
			else if (!grainNumbers.isEmpty() && !shallSpeak)
			{
				speakerState = StreamState::Inactive;
			}
			else
			{
				speakerState = StreamState::NoGrainsInBuffer;
			}

			break;
		case StreamState::NoGrainsInBuffer:
			position = 0;
		}
	}

	// generates a random pause
	void generatePause(int fs) {
		pauseLength = random.nextInt((int)(.05 * fs)) + (int)(1.0 * fs);
	}

	void resetBuffer()
	{
		grainNumbers.reset();
	}

};




#endif  // VOICE_H_INCLUDED
