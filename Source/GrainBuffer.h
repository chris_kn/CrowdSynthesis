/*
  ==============================================================================

    GrainBuffer.h
    Created: 27 Jul 2017 3:57:30pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include"Ringbuffer.h"

#define NOTPAUSE_PROBABILITY 0.95f

class GrainBuffer : public Ringbuffer
{/// Class holding the grain buffer for speaker or voices of a conversation and some additional information
public:
	GrainBuffer()
		: timeUnstressed(0),
		grains_since_last_pause(0),
		firstGrainRMS(0),
		secondGrainRMS(0),
		min_grains_before_pause(4),
		lastGrainStressed(false),
		notPauseProbability(NOTPAUSE_PROBABILITY),
		lastGrainLength(0)
	{

	}
	
	~GrainBuffer() {}


	int timeUnstressed;
	bool lastGrainStressed;
	int grains_since_last_pause;
	int firstGrainRMS;
	int secondGrainRMS;
	int min_grains_before_pause;
	float notPauseProbability;
	int lastGrainLength;

private:
	
};

