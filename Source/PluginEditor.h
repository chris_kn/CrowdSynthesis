/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "XYSlider.h"
#include "SpeakerRoutingLine.h"
#include "CSLookAndFeel.h"
#include "SpeakerIcon.h"
#include "SpeakerIconLine.h"
#include "VoiceSelectPalette.h"
#include "ConversationPool.h"
#include "DeleteButton.h"
#include "KnobRoutingLine.h"

//==============================================================================
/**
*/
class Cs_plugInAudioProcessorEditor  : public AudioProcessorEditor, public SliderListener, public ButtonListener, public ChangeListener, public DragAndDropContainer
/// The main GUI component. Contains all the other components and updates the host's parameters if values were changed by the user.
{
public:
    Cs_plugInAudioProcessorEditor (Cs_plugInAudioProcessor&, AudioProcessorValueTreeState&);
	~Cs_plugInAudioProcessorEditor();

	typedef AudioProcessorValueTreeState::SliderAttachment SliderAttachment;
	typedef AudioProcessorValueTreeState::ButtonAttachment ButtonAttachment;

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
	void sliderValueChanged(Slider* sliderThatWasMoved) override;
	void sliderDragStarted(Slider* sliderBeingDragged) override;
	void sliderDragEnded(Slider* sliderBeingDragged) override;
	void buttonClicked(Button* buttonThatWasClicked) override;

	void changeListenerCallback(ChangeBroadcaster * source) override;

	void updateGUI2ChangedParams(DataState datastate);

	//void updateSpeakerSelectState();

	//void updateConvSelectState();

	//void updateTextFields();

	//void updateConvTextFields();

	Array<Colour> colors;

private:
	// Functions =====
	void setBoundsConvSetup();
	void updateConvPool();
	void updateSpeakerPanel();
	void updateRoutingPanel();
	// Internal Parameters and Vectors
	Cs_plugInAudioProcessor& processor;
	AudioProcessorValueTreeState& valueTreeState;
	ChangeBroadcaster* changeB;
	int voiceSlider_dragged;
	bool create_new;

	// GUI COMPONENTS ============

	// General components

	SharedResourcePointer<TooltipWindow> tooltipWin;
	CSLookAndFeel csLookAndFeel;
	LookAndFeel_V4 lookAndFeel4;
	Label * infoText;
    
	ScopedPointer<GroupComponent> general_settings_group;
	ScopedPointer<TextButton> openB;

	Slider gain_slider;
	ScopedPointer<SliderAttachment> gainAttachment;
	ScopedPointer<Label> gain_slider_label;

	Slider num_channels_slider;
	ScopedPointer<SliderAttachment> num_chan_attachment;
	ScopedPointer<Label> num_channels_slider_label;

	// x/y-plane components
	ScopedPointer<GroupComponent> xyGroup;
	Slider xSpread_slider;
	ScopedPointer<SliderAttachment> xspread_attachment;
	Slider ySpread_slider;
	ScopedPointer<SliderAttachment> yspread_attachment;
	ScopedPointer<XYSlider> xy_plane;

	// conv setup panel
	ScopedPointer<GroupComponent> convSetupGroup;
	ScopedPointer<Viewport> voiceSelectViewport;
	ScopedPointer<VoiceSelectPalette> voiceSelectPalette;
	ScopedPointer<TextButton> addConvButton;
	ScopedPointer<DeleteButton> deleteButton;
	ScopedPointer<Viewport> convPoolViewport;
	ScopedPointer<ConversationPool> convPool;

	// Single speaker panel
	ScopedPointer<GroupComponent> singleSpeakerGroup;
	ScopedPointer<Viewport> singleSpeakerViewport;
	ScopedPointer<SpeakerIconLine> singleSpeakerPanel;
	ScopedPointer<TextButton> addRandomSpeakerButton;

	// Routing Panel
	ScopedPointer<GroupComponent> routingGroup;
	ScopedPointer<TextButton> placeAllRandomlyButton;
	ScopedPointer<TextButton> placeRandomlyButton;
	ScopedPointer<Viewport> routingViewport;
	KnobRoutingLine* routingPanel;
	//ScopedPointer<KnobRoutingLine> routingPanel;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Cs_plugInAudioProcessorEditor)
};
