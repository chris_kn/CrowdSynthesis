/*
  ==============================================================================

    NewConversation.h
    Created: 12 Jul 2017 4:50:32pm
    Author:  C. Knörzer

  ==============================================================================
*/


#pragma once
#include "TalkingStream.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "Grainbuffer.h"


#define NOTPAUSE_PROBABILITY 0.95f

class NewConversation : public TalkingStream
///Conversation object handling one stream of glibberish talk with changing voice simulating a conversation.
/**
The voice is changed randomly after x grains causing the ringbuffer to be cleared. It can hold only ONE instance
of each available voice to avoid two identical voices talking to each other.

It contains a "chattiness"-float, a factor that determines the probability of how often the voice changes.

*/
{
public:
	NewConversation()
		: activeVoice(-1),
		chattiness(.6f)
	{
		set_min_grains_before_pause((int)(chattiness.get() * 20.f));
		currUtteranceLength = getUtteranceLength();
	}
	NewConversation(int channel, float level, int numVoices)
		: activeVoice(-1),
		chattiness(.6f)
	{
		for (int i = 0; i < numVoices; ++i)
		{
			grainbuffers.push_back(GrainBuffer());
			timesUnstressed.add(0);
		}
		channels.add(channel);
		levels.add(level);
		set_min_grains_before_pause((int)(chattiness.get() * 20.f));
		currUtteranceLength = getUtteranceLength();
	}

	NewConversation(Array<int> newchannels, Array<float>newlevels, Array<int>newvoices, int numVoices, float newchattiness = 0.6f)
		: TalkingStream(newchannels, newlevels),
		voices(newvoices)
	{
		chattiness.set(newchattiness);
		for (int i = 0; i < numVoices; ++i)
		{
			grainbuffers.push_back(GrainBuffer());
			timesUnstressed.add(0);
		}
		set_min_grains_before_pause((int)(chattiness.get() * 20.f));
		currUtteranceLength = getUtteranceLength();
		activeVoice = newvoices[random.nextInt(newvoices.size())];
	}

	~NewConversation()
	{
		voices.clear();
	}

	// checks if buffer is not empty
	bool isReadyToPlay()
	{
		return !grainbuffers[activeVoice].isEmpty();
	}

	// check if grain ring buffer is full
	bool isBufferFull(int voicenum)
	{
		return grainbuffers[voicenum].isFull();
	}

	bool pushGrain(int voicenum, int grainnum)
	{
		if(hasVoice(voicenum))
		{
			return grainbuffers[voicenum].tryPush(grainnum);
		}
		else
		{
			return false;
		}
	}

	GrainBuffer & getBuffer()
	{
		return grainbuffers[activeVoice];
	}

	void addVoice(int number)
	{
		if (voices.addIfNotAlreadyThere(number))
		{
			pitches.add(static_cast<Pitch>(random.nextInt(3)));
		}
		changeVoice();
	}

	void addVoice(int number, int pitch)
	{
		if (voices.addIfNotAlreadyThere(number))
		{
			pitches.add(static_cast<Pitch>(pitch));
		}
		changeVoice();
	}

	void addRandomVoice(int numVoices)
	{
		int newVoice = random.nextInt(numVoices);
		if (voices.addIfNotAlreadyThere(newVoice))
		{
			pitches.add(static_cast<Pitch>(random.nextInt(3)));
		}
		changeVoice();
	}
	void removeVoice(int number)
	{
		int index = voices.indexOf(number);
		voices.removeAllInstancesOf(number);
		if (index >= 0 && index < pitches.size())
		{
			pitches.remove(pitches[index]);
		}
		changeVoice();
	}

	void changeVoiceLottery()
	{/// decides if voice is changed or not
		float lottery = random.nextFloat();

		if (lottery < chattiness.get())
		{
			changeVoice();
		}
		else
		{
			currUtteranceLength = getUtteranceLength();
		}
	}

	void changeVoice()
	{/// once the lottery decided the voice has to be changed, this method takes care of it
		Array<int> choice = voices;
		choice.removeAllInstancesOf(activeVoice);
		if (choice.size() > 0)
		{
			activeVoice = choice[random.nextInt(choice.size())];
		}
		else
		{
			activeVoice = voices.size() - 1;
		}
		resetBuffer();
		speakerState = StreamState::NoGrainsInBuffer;
		currUtteranceLength = getUtteranceLength();
		resetRMS();
	}

	int getPitch()
	{/// retuns the "pitch" of the current active voice, that is, if the voice uses the original grains or a pitched version
		int index = voices.indexOf(activeVoice);
		if (index >= 0)
		{
			return pitches[index];
		}
		else
		{
			return 0;
		}
	}

	int size()
	{/// returns the amount of voices of the conversation
		return voices.size();
	}

	bool hasVoice(int voice)
	{/// checks if voice is part of the conversation
		return voices.contains(voice);
	}

	int getVoice()
	{/// returns the active voice number
		return activeVoice;
	}

	int getVoice(int i)
	{/// returns the voice on position i
		if (i < voices.size())
		{
			return voices[i];
		}
		else
		{
			return 0;
		}
	}

	int getLastVoice()
	{/// returns the last voice in the voice vector
		if (voices.size() > 0)
		{
			return voices[voices.size() - 1];
		}
		else
		{
			return -1;
		}
	}

	int getNumVoices()
	{
		return voices.size();
	}

	void prepareNewState(StreamState oldState, int crossFadeLength, double fs)
	{/// orgenizes the transfer from one state to another (between syllables or pauses)
		switch (oldState)
		{
		case StreamState::PlaySyll:

			oldSyllable = newSyllable;
			fadePosition = 0;
			--currUtteranceLength;

			if (!grainbuffers[activeVoice].isEmpty() && shallSpeak && currUtteranceLength > 0)
			{
				newSyllable = grainbuffers[activeVoice].pop();
				speakerState = StreamState::Crossfade;
			}
			if (grainbuffers[activeVoice].isEmpty() || newSyllable == -1 || !shallSpeak || currUtteranceLength < 1)
			{
				speakerState = StreamState::Fadeout;
				generatePause(fs);
			}

			break;
		case StreamState::Crossfade:
			position = crossFadeLength;
			speakerState = StreamState::PlaySyll;
			old_RMS_factor = new_RMS_factor;
			break;
		case StreamState::Fadeout:
			position = 0;
			speakerState = StreamState::Pausing;
			resetRMS();
			break;
		case StreamState::Pausing:
			if (currUtteranceLength < 1)
			{
				changeVoiceLottery();
			}
			position = 0;
			oldSyllable = newSyllable;

			if (!grainbuffers[activeVoice].isEmpty() && shallSpeak)
			{
				newSyllable = grainbuffers[activeVoice].pop();

				if (newSyllable < 0)
				{
					speakerState = StreamState::Pausing;
					generatePause(fs);
				}
				else
				{
					speakerState = StreamState::PlaySyll;
				}
			}
			else if (!grainbuffers[activeVoice].isEmpty() && !shallSpeak)
			{
				speakerState = StreamState::Inactive;
			}
			else
			{
				speakerState = StreamState::NoGrainsInBuffer;
			}

			break;
		case StreamState::NoGrainsInBuffer:
			position = 0;
		}
	}

	void getNextSyllable()
	{/// returns the next grain number from the buffer, if the buffer is not empty

		oldSyllable = newSyllable;

		if (!grainbuffers[activeVoice].isEmpty())
		{
			newSyllable = grainbuffers[activeVoice].pop();
		}
		else
		{
			speakerState = StreamState::NoGrainsInBuffer;
		}
	}

	void generatePause(int fs)
	{/// calculates randomly the length of the next pause, which can be a short pause under 1s or a long pause over 1s
		float lottery = random.nextFloat();
		float limit = (1.f - chattiness.get()) / 2;

		if (lottery > limit) // generate short pause
		{
			pauseLength = (fs / 10) + (int)(random.nextFloat() * fs * (1 - chattiness.get()));
		}
		else // this will be a long, almost embarrassing pause
		{
			pauseLength = fs + (int)(random.nextFloat() * 6 * fs * (1 - chattiness.get()));	
		}
		
	}

	void setChattiness(float value)
	{
		if (value > 0.f && value <= 1.f)
		{
			chattiness.set(value);
		}
		else if (value < 0.f)
		{
			chattiness.set(0.f);
		}
		else if (value > 1.f)
		{
			chattiness.set(1.f);
		}
		
		for (int i = 0; i < grainbuffers.size(); ++i)
		{
			grainbuffers[i].min_grains_before_pause = (int)(chattiness.get() * 20.f);
		}

	}

	void resetBuffer()
	{
		for (int b = 0; b < grainbuffers.size(); ++b)
		{
			grainbuffers[b].reset();
		}
	}

	// state of the current conversation
	Array<Pitch> pitches;
	Array<int> voices;
	int activeVoice;
	Atomic<float> chattiness;
	Array<int> timesUnstressed;

private:

	std::vector<GrainBuffer> grainbuffers;

	int getUtteranceLength()
	{
		return random.nextInt(20) + get_min_grains_before_pause();
	}
	
	int currUtteranceLength;
	
};
