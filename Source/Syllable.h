/*
  ==============================================================================

    Syllable.h
    Created: 10 Mar 2017 11:29:31am
    Author:  C. Knörzer

  ==============================================================================
*/

#ifndef SYLLABLE_H_INCLUDED
#define SYLLABLE_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class Syllable
///Syllable or grain object, containing 3 audiobuffer objects (normal pitch, higher pitch, lower pitch) and acoustic parameters as well as the coordinates in the valence- / arrousal plain.
{
public:Syllable()
	:xPos(0.f),
	 yPos(0.f)
	{
		
	}
public:Syllable(AudioBuffer<float> input, double sampleR, float xpos, float ypos, float pitchHz, float pitchshift)
	{
		audio.push_back(input);
		audio.push_back(pitchAudio(input, pitchshift));
		audio.push_back(pitchAudio(input, -pitchshift));
		fs = sampleR;
		rms = audio[0].getRMSLevel(0, 0, audio[0].getNumSamples());
		//length = audio.getNumSamples();
		xPos = xpos;
		yPos = ypos;
		pitch = pitchHz;
	};
	   ~Syllable()
	   {
		   audio.clear();
	   }

	bool operator < (const Syllable& str) const
	{/// function called when sorting Syllable objects. As implemented here, Syllable objects are sorted by pitch
		return (pitch < str.pitch);
	}

	bool operator > (const Syllable& str) const
	{/// function called when sorting Syllable objects. As implemented here, Syllable objects are sorted by pitch
		return (pitch > str.pitch);
	}

	   std::vector<AudioBuffer<float>> audio;
	   float xPos;
	   float yPos;

	   double fs;
	   float pitch;
	   float rms;

//====================================
private:
	AudioBuffer<float> pitchAudio(AudioBuffer<float> original, float semitones)
	{/// function pitching an audio input vector up- or downwards by linear interpolation
		double step = pow(2.0 ,(double)semitones * (1.0 / 12.0));
		double pos = 0.0;
		int prevPos = 0;
		int nextPos = 0;
		//int origLength = original.getNumSamples();
		AudioBuffer<float> newAudio(1, (int)((float)original.getNumSamples() / step));
		
		for (int i = 0; i < newAudio.getNumSamples(); ++i)
		{
			pos = step * (double)i;
			prevPos = (int)floorf(pos);
			nextPos = (int)ceilf(pos);
			if (nextPos < original.getNumSamples())
			{
				float prevValue = original.getSample(0, prevPos);
				float nextValue = original.getSample(0, nextPos);
				newAudio.setSample(0, i, prevValue + (nextValue - prevValue) * (pos - floor(pos)));
			}
			else
			{
				newAudio.setSample(0, i, original.getSample(0, prevPos));
			}
			
		}
		return newAudio;
	}
};





#endif  // SYLLABLE_H_INCLUDED
