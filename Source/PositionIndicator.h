/*
  ==============================================================================

    PositionIndicator.h
    Created: 8 Aug 2017 6:40:04pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class PositionIndicator    : public Component
{/// GUI component. Displays the current y position and x and y range in the coordinate system
public:
    PositionIndicator()
		: xpos(0.f),
		ypos(0.f),
		yspread(0.f)
    {

    }

    ~PositionIndicator()
    {
    }

	void updatePosition(float x, float y)
	{
		xpos = x;
		ypos = y;
		this->repaint();
	}

	void updateYSpread(float s)
	{
		yspread = s;
		this->repaint();
	}

    void paint (Graphics& g) override
    {

        g.setColour (Colours::grey);
        
        g.setFont (12.0f);
        
		g.drawText("X: " + String(xpos) + " Y: " + String(ypos) + " YS: " + String(yspread), getLocalBounds(), Justification::bottomLeft, true);

    }

    void resized() override
    {

    }

private:
	float xpos;
	float ypos;
	float yspread;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PositionIndicator)
};
