/*
  ==============================================================================

    Ringbuffer.h
    Created: 13 Mar 2017 8:21:21pm
    Author:  C. Knörzer

  ==============================================================================
*/

#ifndef RINGBUFFER_H_INCLUDED
#define RINGBUFFER_H_INCLUDED

#define RING_BUFFER_SIZE 20

#include "../JuceLibraryCode/JuceHeader.h"

class Ringbuffer
	/// ringbuffer, thread safe for single access. Stores the next syllables. A value of -1 represents a pause.
{
public:
	Ringbuffer()
		: size(RING_BUFFER_SIZE)
	{
		writePointer.set(0);
		readPointer.set(0);
	}

	bool tryPush(int val)
	{
		const auto current_tail = writePointer.get();
		const auto next_tail = increment(current_tail);

		if (next_tail != readPointer.get())
		{
			buffer[current_tail] = val;
			writePointer.set(next_tail);
			return true;
		}
		
		return false;
	}

	void push(int val)
	{
		while (!tryPush(val));
	}

	bool tryPop(int* element)
	{
		const auto currentHead = readPointer.get();

		if (currentHead != writePointer.get())
		{
			*element = buffer[currentHead];
			readPointer.set(increment(currentHead));
			return true;
		}

		return false;
	}

	int pop()
	{
		int ret;
		while (!tryPop(&ret));
		return ret;
	}

	bool isFull()
	{
		const auto current_tail = writePointer.get();

		if (increment(current_tail) != readPointer.get())
		{
			return false;
		}
		else
		{
			return true;
		}

	}
	bool isEmpty()
	{
		const auto currentHead = readPointer.get();
		if (currentHead == writePointer.get() || buffer[currentHead] < -1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void reset()
	{
		readPointer.set(writePointer.get());
	}

private:
	Atomic<int> writePointer{ 0 };
	Atomic<int> readPointer{ 0 };
	int size;
	int buffer[RING_BUFFER_SIZE];

	int increment(int n)
	{
		return (n + 1) % size;
	}
};



#endif  // RINGBUFFER_H_INCLUDED
