/*
  ==============================================================================

    DeleteButton.h
    Created: 24 Oct 2017 10:19:27am
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class DeleteButton    : public Component, public DragAndDropTarget, public ChangeBroadcaster
{/// GUI component: Not really a clickable button, but a rectangle which allows deletions of voices and speaker by
///dropping the respective icons over the rectangle

public:
	DeleteButton()
		: somethingIsBeingDraggedOver(false),
		convToDelete(-1),
		voiceToDelete(-1),
		speakerToDelete(-1)
    {

    }

    ~DeleteButton()
    {
    }

    void paint (Graphics& g) override
    {
		if (!isMouseOverOrDragging())
		{
			g.fillAll (Colours::black.withAlpha(.1f));
		}

        g.setColour (Colours::grey);
        g.drawRect (getLocalBounds(), 1); 

        g.setColour (Colours::white);
       
		Path binpath;
		float middleX = (float)getWidth() / 2.f;
		float middleY = (float)getHeight() / 2.f;

		float bintop = middleY - 8;
		float binbottom = middleY + 8;

		binpath.startNewSubPath(middleX - 7, bintop);
		binpath.lineTo(middleX - 5, binbottom);
		binpath.lineTo(middleX + 5, binbottom);
		binpath.lineTo(middleX + 7, bintop);
		binpath.closeSubPath();

		binpath.startNewSubPath(middleX - 1.7f, binbottom);
		binpath.lineTo(middleX - 2.2f, bintop);
		binpath.startNewSubPath(middleX + 1.7f, binbottom);
		binpath.lineTo(middleX + 2.2f, bintop);

		g.strokePath(binpath, PathStrokeType(1.0f));

		if (somethingIsBeingDraggedOver)
		{
			g.setColour(Colour(169, 100, 100));
			g.drawRect(0, 0, getWidth(), getHeight(), 3);
		}

    }

    void resized() override
    {

    }

	bool isInterestedInDragSource(const SourceDetails & dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().containsAnyOf("cs"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void itemDragEnter(const SourceDetails& dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().containsAnyOf("cs"))
		{
			somethingIsBeingDraggedOver = true;
			repaint();
		}
	}

	void itemDragExit(const SourceDetails& dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().containsAnyOf("cs"))
		{
			somethingIsBeingDraggedOver = false;
			repaint();
		}
	}

	void itemDropped(const SourceDetails& dragSourceDetails)
	{
		String itemName = dragSourceDetails.description.toString();

		if (!itemName.containsAnyOf("sv") && itemName.contains("c") && itemName.containsAnyOf("0123456789"))
		{// delete whole conversation
			somethingIsBeingDraggedOver = false;
			convToDelete = itemName.getTrailingIntValue();
			typeToDelete = DraggedElementType::ConversationIcon;
			sendChangeMessage();
			repaint();
		}
		else if (itemName.contains("c") && itemName.contains("v") && itemName.containsAnyOf("0123456789"))
		{// delete voice from conversation
			somethingIsBeingDraggedOver = false;
			convToDelete = itemName.fromFirstOccurrenceOf("c", false, true).upToFirstOccurrenceOf("v", false, true).getIntValue();
			voiceToDelete = itemName.fromFirstOccurrenceOf("v", false, true).getIntValue();
			typeToDelete = DraggedElementType::ConvParticipantIcon;
			sendChangeMessage();
			repaint();
		}
		else if (!itemName.containsAnyOf("cv") && itemName.contains("s") && itemName.containsAnyOf("0123456789"))
		{// delete single speaker
			somethingIsBeingDraggedOver = false;
			speakerToDelete = itemName.getTrailingIntValue();
			typeToDelete = DraggedElementType::SingleSpeakerIcon;
			sendChangeMessage();
			repaint();
		}

	}

	DraggedElementType typeToDelete;
	int convToDelete;
	int voiceToDelete;
	int speakerToDelete;

private:

	bool somethingIsBeingDraggedOver;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DeleteButton)
};
