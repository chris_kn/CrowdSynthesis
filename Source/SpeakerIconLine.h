/*
  ==============================================================================

    SpeakerIconLine.h
    Created: 23 Oct 2017 11:30:49am
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SpeakerIcon.h"

//==============================================================================
/*
*/
class SpeakerIconLine    : public Component, public DragAndDropTarget, public ChangeBroadcaster
{/// Contains a vector of speaker icons
public:

	SpeakerIconLine(Array<Colour> colorArray, Array<int> voiceNumbers, int pxsize, int direction, int margin = 2)
		/// direction: 0 = horizontal, 1 = vertical
		: pxsize(pxsize),
		iconType(iconType),
		direction(direction),
		selectedIcon(-1),
		margin(margin),
		somethingIsBeingDraggedOver(false),
		speakerToAdd(-1),
		newSelection(false)
	{
		for (int i = 0; i < voiceNumbers.size(); ++i)
		{
			iconArray.add(new SpeakerIcon(DraggedElementType::SingleSpeakerIcon, voiceNumbers[i], colorArray[voiceNumbers[i] % colorArray.size()], -1, i));
			iconArray[i]->addMouseListener(this, false);
			addAndMakeVisible(iconArray[i]);
		}
	}

    ~SpeakerIconLine()
    {
    }

    void paint (Graphics& g) override
    {

        g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

        g.setColour (Colours::grey);
        g.drawRect (getLocalBounds(), 1);   // draw an outline around the component

		if (somethingIsBeingDraggedOver)
		{
			g.setColour(Colour(169, 100, 100));
			g.drawRect(0, 0, getWidth(), getHeight(), 3);
		}

    }

    void resized() override
    {/// handles layout of subcomponents (the icons)

		float startleft = getWidth() / 2.f - (float)pxsize / 2.f;

		for (int i = 0; i < iconArray.size(); ++i)
		{
			iconArray[i]->setBounds(startleft, margin + (pxsize + margin) * i, pxsize, pxsize);
		}
    }

	int getSize()
	{
		return iconArray.size();
	}

	void mouseUp(const MouseEvent& e)
	{
		newSelection = false;

		for (int i = 0; i < iconArray.size(); ++i)
		{
			if (e.eventComponent == iconArray[i] && i != selectedIcon)
			{
				selectedIcon = i;
				newSelection = true;
			}
		}

		if (newSelection)
		{
			for (int i = 0; i < iconArray.size(); ++i)
			{
				iconArray[i]->selected = false;
				iconArray[i]->repaint();
			}

			iconArray[selectedIcon]->selected = true;
			iconArray[selectedIcon]->repaint();
				
			sendChangeMessage();
		}
	}

	int getLength()
	{
		int contentLength = 2*margin + (margin + pxsize) * iconArray.size();
		return std::max(contentLength, pxsize);
	}

	bool isInterestedInDragSource(const SourceDetails & dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().contains("v") && !dragSourceDetails.description.toString().containsAnyOf("cs"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void itemDragEnter(const SourceDetails& dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().contains("v") && !dragSourceDetails.description.toString().containsAnyOf("cs"))
		{
			somethingIsBeingDraggedOver = true;
			repaint();
		}
	}

	void itemDragExit(const SourceDetails& dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().contains("v") && !dragSourceDetails.description.toString().containsAnyOf("cs"))
		{
			somethingIsBeingDraggedOver = false;
			repaint();
		}
	}

	void itemDropped(const SourceDetails& dragSourceDetails)
	{
		String itemName = dragSourceDetails.description.toString();

		if (itemName.contains("v") && !itemName.containsAnyOf("cs"))
		{
			somethingIsBeingDraggedOver = false;
			repaint();
			speakerToAdd = itemName.getTrailingIntValue();
			sendChangeMessage();
		}
	}


	OwnedArray<SpeakerIcon> iconArray;
	DraggedElementType iconType;
	bool somethingIsBeingDraggedOver;
	int pxsize;
	int direction;
	int selectedIcon;
	int margin;
	int speakerToAdd;
	bool newSelection;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SpeakerIconLine)
};
