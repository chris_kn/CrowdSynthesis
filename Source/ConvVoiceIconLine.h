/*
  ==============================================================================

    ConvVoiceIconLine.h
    Created: 25 Oct 2017 10:49:56am
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SpeakerIcon.h"

//==============================================================================
/*
*/
class ConvVoiceIconLine    : public Component
{/// GUI component: The vector of voice icons displayed within a conversaton pad
public:
    ConvVoiceIconLine(Array<Colour> colorArray, Array<int> voiceNumbers, int pxsize, int direction, int convNumber = -1, int margin = 2)
    :pxsize(pxsize),
		direction(direction),
		selectedIcon(-1),
		margin(margin),
		somethingIsBeingDraggedOver(false)
	{
		for (int i = 0; i < voiceNumbers.size(); ++i)
		{
			iconArray.add(new SpeakerIcon(DraggedElementType::ConvParticipantIcon, voiceNumbers[i], colorArray[voiceNumbers[i] % colorArray.size()], convNumber));
			iconArray[i]->addMouseListener(this, false);
			addAndMakeVisible(iconArray[i]);
		}
    }

    ~ConvVoiceIconLine()
    {
    }

    void paint (Graphics& g) override
    {

    }

	void resized() override
	{/// organizes layout of voice icons

		for (int i = 0; i < iconArray.size(); ++i)
		{
			iconArray[i]->setBounds(margin + (pxsize + margin) * i, margin, pxsize, pxsize);
		}
	}

	int getSize()
	{
		return iconArray.size();
	}

	
	int getLength()
	{/// returns an estimate of how much space is needed for the icons
		int contentLength = 2*margin + (margin + pxsize) * iconArray.size();
		return std::max(contentLength, pxsize);
	}

	OwnedArray<SpeakerIcon> iconArray;
	bool somethingIsBeingDraggedOver;
	int pxsize;
	int direction;
	int selectedIcon;
	int margin;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvVoiceIconLine)
};
