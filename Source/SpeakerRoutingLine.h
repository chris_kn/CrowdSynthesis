/*
  ==============================================================================

    SpeakerRoutingLine.h
    Created: 19 Jun 2017 5:03:09pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class SpeakerRoutingLine    : public Component
	/// Container for several text editors representing an output channel to edit the routing of a speaker or conversation
{
public:
    SpeakerRoutingLine(int numChannels)
    {
		if (numChannels > 0)
		{
			for (int chan = 0; chan < numChannels; ++chan)
			{
				gain_txt.add(new TextEditor("channel_gain_" + String(chan)));
				gain_txt_labels.add(new Label("chan_gain_label_" + String(chan), String(chan)));
				gain_txt[chan]->setInputRestrictions(4, "0123456789.");
				addAndMakeVisible(gain_txt[chan]);
				addAndMakeVisible(gain_txt_labels[chan]);
				gain_txt_labels[chan]->attachToComponent(gain_txt[chan], false);
				gain_txt_labels[chan]->setJustificationType(Justification::centredBottom);
			}
		}
		else
		{
			Logger::getCurrentLogger()->writeToLog("Channel Number negative!");
		}
		setSize((numChannels) * 50, 80);
    }

    ~SpeakerRoutingLine()
    {
		gain_txt.clear();
    }

    void paint (Graphics& g) override
    { /// draws the component's outlines
        
        g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

        g.setColour (Colours::grey);
        g.drawRect (getLocalBounds(), 1);   // draw an outline around the component
		
    }

    void resized() override
    {
        /// Sets the bounds of the single text editors within this component
		for (int chan = 0; chan < gain_txt.size(); ++chan)
		{
			gain_txt[chan]->setBounds(40*(chan)+10, 20, 30, 30);
		
		}

    }

	int size()
		/// returns the number of text editors representing an output channel
	{
		return gain_txt.size();
	}

	TextEditor& operator[] (int x) {
		return *gain_txt[x];
	}

	// Params
	OwnedArray<TextEditor> gain_txt;
	OwnedArray<Label> gain_txt_labels;

private:
	
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SpeakerRoutingLine)
};
