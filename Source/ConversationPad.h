/*
  ==============================================================================

    ConversationPad.h
    Created: 20 Oct 2017 6:07:56pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ConvVoiceIconLine.h"

//==============================================================================
/*
*/
class ConversationPad : public Component, public DragAndDropTarget, public ChangeBroadcaster, public ButtonListener
{///displays one conversation in the conversation pool part of the GUI, draggable GUI component
public:
  
	ConversationPad(Array<Colour> speakerColors, Array<int> voices, int pxsize, int convNumber, float chattiness)
		: pxsize(pxsize),
		convNumber(convNumber),
		voiceToAdd(-1),
		somethingIsBeingDraggedOver(false),
		selected(false),
		chattiness(chattiness)
	{

		addRandomButton = new TextButton("+?", "add random voice");
		addRandomButton->addListener(this);
		addAndMakeVisible(addRandomButton);
		speakerViewport = new Viewport("speakerViewport");
		addAndMakeVisible(speakerViewport);
		speakerIcons = new ConvVoiceIconLine(speakerColors, voices, pxsize, 0, convNumber, 3);
		speakerViewport->setViewedComponent(speakerIcons);
		chattinessSlider = new Slider();
		chattinessSlider->setRange(0.1, 1.0, 0.05);
		chattinessSlider->setSliderStyle(Slider::SliderStyle::LinearVertical);
		chattinessSlider->setTextBoxStyle(Slider::NoTextBox, true, 0, 0);
		addAndMakeVisible(chattinessSlider);
		chattinessSlider->setValue(chattiness);

		setName("ConvPad");
		
	}

    ~ConversationPad()
    {
    }

    void paint (Graphics& g) override
    {/// paints the component
		// change background color if component is selected
		if (selected)
		{
			g.fillAll(Colours::lightgrey.withAlpha(.5f));
		}
		// change colors of lines if sth is being dragged over or mouse is over component
		if (isMouseOverOrDragging() || somethingIsBeingDraggedOver)
		{
			g.setColour(Colours::lightgrey);
		}
		else if (selected)
		{
			g.setColour(Colours::darkgrey);
		}
		else
		{
			g.setColour (Colours::grey);
		}
        
		g.fillRect(36, 2, 5, getHeight() - 4);
		g.fillRect(getWidth() - 42, 2, 5, getHeight() - 4);
		g.drawLine(0.5f, 0.5f, getWidth() - 1.f, 0.5f);
		g.drawLine(0.5f, getHeight() - 0.5f, getWidth() - 1.f, getHeight() - 0.5f);

		// draw red rectangle if voice symbol is being dragged over
		if (somethingIsBeingDraggedOver)
		{
			g.setColour(Colour(169,100,100));
			g.drawRect(36, 0, getWidth()-78, getHeight(), 3);
		}

        
    }

    void resized() override
    {/// organizes the layout of subcomponents
        
		Rectangle<int> area = getLocalBounds();
		int margin = 5;
		int chSlWidth = 30;
		int rndButtonWidth = 30;
		int  rectWidth = 5;
		int viewStart = 2 + rndButtonWidth + margin + rectWidth + margin;
		int viewEnd = getWidth() - margin - rectWidth - margin - chSlWidth;

		addRandomButton->setBounds(2, 2, rndButtonWidth, getHeight()-4);
		chattinessSlider->setBounds(getWidth()-32, 0, 30, getHeight());
		speakerViewport->setBounds(viewStart, 0, viewEnd - viewStart, getHeight());
		speakerIcons->setBounds(1, 1, std::max(speakerIcons->getLength(), getWidth()), getHeight()-2);

    }

	int getNumVoices()
	{/// returns number of voice symbols in the conversation pad
		return speakerIcons->getSize();
	}

	void mouseDrag(const MouseEvent & event)
	{
		DragAndDropContainer* container = DragAndDropContainer::findParentDragContainerFor(this);

		if (container != nullptr)
		{
			if (event.eventComponent == this)
			{
				container->startDragging("c" + String(convNumber), this);
			}
		}
	}

	bool isInterestedInDragSource(const SourceDetails & dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().contains("v") && !dragSourceDetails.description.toString().contains("c"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void itemDragEnter(const SourceDetails& dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().contains("v") && !dragSourceDetails.description.toString().contains("c"))
		{
			somethingIsBeingDraggedOver = true;
			repaint();
		}
	}

	void itemDragExit(const SourceDetails& dragSourceDetails)
	{
		if (dragSourceDetails.description.toString().contains("v") && !dragSourceDetails.description.toString().contains("c"))
		{
			somethingIsBeingDraggedOver = false;
			repaint();
		}
	}

	void itemDropped(const SourceDetails& dragSourceDetails)
	{
		String itemName = dragSourceDetails.description.toString();

		if (itemName.contains("v") && !itemName.contains("c"))
		{
			somethingIsBeingDraggedOver = false;
			repaint();
			voiceToAdd = itemName.getTrailingIntValue();
			sendChangeMessage();

		}
	}

	void buttonClicked(Button * buttonThatWasClicked)
	{
		if (buttonThatWasClicked == addRandomButton)
		{
			voiceToAdd = -1;
			sendChangeMessage();
		}
	}

	void mouseEnter(const MouseEvent& e)
	{
		repaint();
	}
	void mouseExit(const MouseEvent& e)
	{
		repaint();
	}

	// child GUI components
	ScopedPointer<TextButton> addRandomButton;
	ScopedPointer<Viewport> speakerViewport;
	ScopedPointer<ConvVoiceIconLine> speakerIcons;
	ScopedPointer<Slider> chattinessSlider;
	
	// parameters
	bool somethingIsBeingDraggedOver;
	int pxsize;
	int convNumber;
	int voiceToAdd;
	bool selected;
	float chattiness;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConversationPad)
};
