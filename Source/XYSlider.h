/*
  ==============================================================================

    XYSlider.h
    Created: 8 Jun 2017 7:54:29pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "XYSelector.h"
#include "PositionIndicator.h"

//==============================================================================
/*
*/
class XYSlider  : public Component, public ChangeBroadcaster
	/// a two-dimensional slider with an adaptable rectangle covering a selected, two-dimensional range
{
public:
	//XYSlider();
	XYSlider(float, float, float, float);
	~XYSlider();

    void paint (Graphics&) override;
	//void paintDataSpot(float, float, Colour);
    void resized() override;
	int getXMiddle();
	int getYMiddle();

	float xpos;
	float ypos;

	void setXSpread(float);
	void setYSpread(float);

	void setSliderPos(float, float);

	int dragCondition;
	float normX;
	float normY;

	int voiceSlider_dragged;
	Array<Colour> spotcolors;

	bool isDataLoaded;
	std::vector<float> spotsX;
	std::vector<float> spotsY;
	std::vector<int> spotsVoice;
	ScopedPointer<PositionIndicator> pos_indicator;

private:

	ScopedPointer<XYSelector> xypoint;
	ComponentDragger myDragger;

	void mouseDown(const MouseEvent& event) override;
	void mouseUp(const MouseEvent & event) override;
	void mouseDrag(const MouseEvent & event) override;
	void mouseWheelMove(const MouseEvent & event, const MouseWheelDetails & wheel) override;

	float pix2SlideValX(float);
	float pix2SlideValY(float);

	float slideValX2pix(float slideX);
	float slideValY2pix(float slideY);

	float pixPosX;
	float pixPosY;
	float pixSpreadX;
	float pixSpreadY;

	float xMin;
	float yMin;
	float xMax;
	float yMax;
	
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (XYSlider)
};
