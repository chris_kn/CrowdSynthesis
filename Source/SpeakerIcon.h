/*
  ==============================================================================

    SpeakerIcon.h
    Created: 20 Oct 2017 6:07:14pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

enum DraggedElementType
{
	ConversationIcon = 0,
	VoiceToSelectIcon,
	ConvParticipantIcon,
	SingleSpeakerIcon
};

//==============================================================================
/*
*/
class SpeakerIcon    : public Component
{/// GUI component: Draws an icon consisting of a rectangle and a circle and stores information on the icon
public:
    SpeakerIcon(DraggedElementType draggedElementType,  int voice, Colour color, int convNumber = -1, int speakerNumber = -1)
		: draggedElementType(draggedElementType),
		selected(false),
		voice(voice),
		color(color),
		convNumber(convNumber),
		speakerNumber(speakerNumber)
    {

    }

    ~SpeakerIcon()
    {
    }

    void paint (Graphics& g) override
    {/// draws the speaker icon

        g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

        g.setColour (color);

		if (selected)
		{
			g.fillRect (getLocalBounds());   // draw an outline around the component
			g.setColour(getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
		}

		float halfx = getWidth() / 2.f;
		float radius = getWidth() / 6.f;
		float rwidth = getWidth() / 10.f * 8.f;
		float rheight = getHeight() * 3.f / 10.f;

		g.drawEllipse(halfx - radius, getHeight() / 3.f - radius, 2.f * radius, 2.f * radius, 1.f);
		g.drawRect(halfx - rwidth / 2.f, .6f * getHeight(), rwidth, rheight, 1.f);

        
    }

    void resized() override
    {

    }

	void select()
	{
		selected = true;
		repaint();
	}

	void deselect()
	{
		selected = false;
		repaint();
	}

	void mouseDrag(const MouseEvent& event)
	{
		DragAndDropContainer* container = DragAndDropContainer::findParentDragContainerFor(this);

		if (container != nullptr)
		{
			if (draggedElementType == DraggedElementType::ConvParticipantIcon && convNumber > -1)
			{
				container->startDragging("c"+ String(convNumber) + "v" + String(voice), this);
			}
			else if (draggedElementType == DraggedElementType::SingleSpeakerIcon && convNumber == -1)
			{
				container->startDragging("s" + String(speakerNumber), this);
			}
			else if (draggedElementType == DraggedElementType::VoiceToSelectIcon && convNumber == -1 && speakerNumber ==-1)
			{
				container->startDragging("v" + String(voice), this);
			}
			
		}
	}

	bool selected;
	
	DraggedElementType draggedElementType;
	int voice;
	Colour color;
	int convNumber;
	int speakerNumber;
	


private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SpeakerIcon)
};
