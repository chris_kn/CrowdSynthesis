/*
  ==============================================================================

	This file was auto-generated!

	It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//#define NUMBERVOICES 10
#define MAXSPEAKER 20

//==============================================================================
Cs_plugInAudioProcessorEditor::Cs_plugInAudioProcessorEditor(Cs_plugInAudioProcessor& p, AudioProcessorValueTreeState& vTS)
	: AudioProcessorEditor(&p),
	processor(p),
	valueTreeState(vTS),
	create_new(true)
	//Thread("checkIfLoaded"),
{
	colors = {
		Colour(130, 159, 206),
		Colour(188, 107, 107),
		Colour(209, 197, 94),
		Colour(107, 183, 82),
		Colour(82, 183, 159),
		Colour(148, 139, 186),
		Colour(186, 128, 129),
		Colour(91, 106, 140),
		Colour(122, 140, 91),
		Colour(201, 168, 98)
	};

	// set up x/y-plane components
	addAndMakeVisible(xyGroup = new GroupComponent("xyGroup", "Set allowed x and y values"));

	xyGroup->addAndMakeVisible(xSpread_slider);
	xspread_attachment = new SliderAttachment(valueTreeState, "curr_valence_spread", xSpread_slider);
	xSpread_slider.setSliderStyle(Slider::LinearBar);
	xSpread_slider.setLookAndFeel(&lookAndFeel4);
	xSpread_slider.addListener(this);

	xyGroup->addAndMakeVisible(ySpread_slider);
	yspread_attachment = new SliderAttachment(valueTreeState, "curr_arousal_spread", ySpread_slider);
	ySpread_slider.setSliderStyle(Slider::LinearBarVertical);
	ySpread_slider.setLookAndFeel(&lookAndFeel4);
	ySpread_slider.addListener(this);

	xyGroup->addAndMakeVisible(xy_plane = new XYSlider(-100.f, 100.f, -100.f, 100.f));
	xy_plane->spotcolors = colors;
	xy_plane->addChangeListener(this);

	// add general GUI components
	addAndMakeVisible(general_settings_group = new GroupComponent("general_settings_group", "General"));
	general_settings_group->addAndMakeVisible(openB = new TextButton("openB"));
	
	openB->addListener(this);

	general_settings_group->addAndMakeVisible(gain_slider);
	gainAttachment = new SliderAttachment(valueTreeState, "gainParam", gain_slider);
	gain_slider.setSliderStyle(Slider::Rotary);
	gain_slider.setTextBoxStyle(Slider::TextBoxBelow, false, 80, 20);
	gain_slider_label = new Label("gain_slider_label", "Gain");
	gain_slider_label->attachToComponent(&gain_slider, false);
	gain_slider_label->setJustificationType(Justification::centred);

	general_settings_group->addAndMakeVisible(num_channels_slider);
	num_chan_attachment = new SliderAttachment(valueTreeState, "target_num_outputs", num_channels_slider);
	num_channels_slider.setSliderStyle(Slider::IncDecButtons);
	num_channels_slider.setTooltip("Set Number of Output Channels");
	num_channels_slider_label = new Label("num_channels_slider_label", "Channels");
	num_channels_slider_label->attachToComponent(&num_channels_slider, false);
	num_channels_slider_label->setJustificationType(Justification::centred);

	// CONVERSATION SETUP
	addAndMakeVisible(convSetupGroup = new GroupComponent("convSetupGroup", "Conversation Setup"));
	convSetupGroup->addAndMakeVisible(voiceSelectViewport = new Viewport("voiceSelectViewport"));
	voiceSelectViewport->setViewedComponent(voiceSelectPalette = new VoiceSelectPalette(10, colors, 30));
	convSetupGroup->addAndMakeVisible(addConvButton = new TextButton("+Conv", "add conversation"));
	addConvButton->addListener(this);
	convSetupGroup->addAndMakeVisible(deleteButton = new DeleteButton());
	deleteButton->addChangeListener(this);
	convSetupGroup->addAndMakeVisible(convPoolViewport = new Viewport("convPoolViewport"));
	if (processor.datastate == Loaded)
	{
		updateConvPool();
	}

	// SINGLE SPEAKER PANEL
	addAndMakeVisible(singleSpeakerGroup = new GroupComponent("singleSpeakerGroup", "SiSp"));
	singleSpeakerGroup->addAndMakeVisible(singleSpeakerViewport = new Viewport("singleSpeakerViewport"));
	singleSpeakerGroup->addAndMakeVisible(addRandomSpeakerButton = new TextButton("+ ?", "add random speaker"));
	addRandomSpeakerButton->addListener(this);
	updateSpeakerPanel();

	// ROUTING PANEL
	addAndMakeVisible(routingGroup = new GroupComponent("routingGroup", "Out"));
	routingGroup->addAndMakeVisible(placeRandomlyButton = new TextButton("-> ?", "place on one random channel"));
	placeRandomlyButton->addListener(this);
	routingGroup->addAndMakeVisible(placeAllRandomlyButton = new TextButton("All->?", "place all speakers/conversations randomly"));
	placeAllRandomlyButton->addListener(this);
	routingGroup->addAndMakeVisible(routingViewport = new Viewport());
	updateRoutingPanel();

	// register this editor to be one of the processor's change listeners
	processor.addChangeListener(this);

	// window size
	setSize(820, 330);
	
	// use customized look
	this->setLookAndFeel(&csLookAndFeel);

	// go through list to update GUI components
	updateGUI2ChangedParams(processor.datastate);

}

Cs_plugInAudioProcessorEditor::~Cs_plugInAudioProcessorEditor()
{
	
	deleteButton->removeChangeListener(this);
	placeRandomlyButton->removeListener(this);
	placeAllRandomlyButton->removeListener(this);
	processor.removeChangeListener(this);
	xy_plane->removeChangeListener(this);
	this->setLookAndFeel(nullptr);
	
}

//==============================================================================
void Cs_plugInAudioProcessorEditor::paint(Graphics& g)
{/// fills background
	
	g.fillAll(Colour(0xff323e44));
	
}

void Cs_plugInAudioProcessorEditor::resized()
{/// sets the GUI component bounds

	// x/y-plane
	xyGroup->setBounds(10, 7, 300, 310);
		xy_plane->setBounds(15, 25, 250, 250);
		ySpread_slider.setBounds(270, 25, 15, 250);
		xSpread_slider.setBounds(15, 280, 250, 15);
		xy_plane->setSliderPos(*valueTreeState.getRawParameterValue("curr_valence"), *valueTreeState.getRawParameterValue("curr_arousal"));
		xy_plane->setXSpread(*valueTreeState.getRawParameterValue("curr_valence_spread"));
		xy_plane->setYSpread(*valueTreeState.getRawParameterValue("curr_arousal_spread"));
	// general components
	general_settings_group->setBounds(705, 7, 100, 310);
		openB->setBounds(10, 20, 80, 75);
		gain_slider.setBounds(10, 128, 80, 80);
		num_channels_slider.setBounds(10, 245, 80, 55);

	// conv setup
	convSetupGroup->setBounds(320, 7, 190, 310);
		voiceSelectViewport->setBounds(5, 20, 180, 75);
		voiceSelectPalette->setBounds(5, 5, 165, voiceSelectPalette->returnHeight());
		addConvButton->setBounds(5, 90, 85, 30);
		deleteButton->setBounds(95, 90, 90, 30);
		convPoolViewport->setBounds(5, 125, 180, 210);
		
	// Single Speaker panel
	singleSpeakerGroup->setBounds(520, 7, 60, 310);
		addRandomSpeakerButton->setBounds(5, 20, 50, 25);
		singleSpeakerViewport->setBounds(5, 50, 50, 250);
	
	// ROUTING PANEL
	routingGroup->setBounds(590, 7, 105, 310);
		placeRandomlyButton->setBounds(5, 20, 95, 30);
		placeAllRandomlyButton->setBounds(5, 55, 95, 30);
		routingViewport->setBounds(5, 95, 95, 210);
		updateRoutingPanel();

}


void Cs_plugInAudioProcessorEditor::sliderValueChanged(Slider * sliderThatWasMoved)
{/// is called whenever a alider value changed to update other gui components and to change processor parameters


	if (sliderThatWasMoved == &xSpread_slider)
	{
		xy_plane->setXSpread(xSpread_slider.getValue());
		processor.mustRefreshSelGrainMatrix = 1;
		return;
	}
	else if (sliderThatWasMoved == &ySpread_slider)
	{
		xy_plane->setYSpread(ySpread_slider.getValue());
		processor.mustRefreshSelGrainMatrix = 1;
		return;
	}

	if (processor.datastate != Loaded) return;
	if (routingViewport->getViewedComponent() == nullptr)return;

	for (int i = 0; i < routingPanel->knobs.size(); ++i)
	{
		if (sliderThatWasMoved == routingPanel->knobs[i] && processor.sel_conv.get() > -1 && i < processor.getTotalNumOutputChannels())
		{
			processor.newConvPanningChannel = i;
			processor.newConvPanningLevel = (float)routingPanel->knobs[i]->getValue();
			processor.areThereNewConvPanningValues.set(1);
			return;
		}
		else if (sliderThatWasMoved == routingPanel->knobs[i] && processor.sel_speaker.get() > -1 && processor.getTotalNumOutputChannels())
		{
			processor.newPanningChannel.set(i);
			processor.newPanningLevel.set((float)routingPanel->knobs[i]->getValue());
			processor.areThereNewPanningValues.set(1);
			return;
		}
	}
}

void Cs_plugInAudioProcessorEditor::sliderDragStarted(Slider * sliderBeingDragged)
{/// used to higlight the dots of the currently dragged speaker slider in the x/y-plane in a future release
	
}

void Cs_plugInAudioProcessorEditor::sliderDragEnded(Slider * sliderBeingDragged)
{/// used to stop the highlighting of the until then dragged speaker slier in a future release

}

void Cs_plugInAudioProcessorEditor::buttonClicked(Button * buttonThatWasClicked)
{ /// called when a button was clicked
	if (buttonThatWasClicked == openB)
	{
		processor.initiateLoading();	
	}
	else if (buttonThatWasClicked == addConvButton)
	{
		if (processor.datastate != Loaded) return;
		processor.addConversation();
	}
	else if (buttonThatWasClicked == addRandomSpeakerButton)
	{
		if (processor.datastate != Loaded) return;

		processor.random_speaker_to_add.add(1);

	}
	else if (buttonThatWasClicked == placeRandomlyButton)
	{
		if (processor.datastate != Loaded) return;

		if (processor.sel_conv.get() > -1)
		{
			processor.placeConvRandomly();
		}
		else if (processor.sel_speaker.get() > -1)
		{
			processor.placeOneSpeakerRandomly();
		}
	}
	else if (buttonThatWasClicked == placeAllRandomlyButton)
	{
		if (processor.datastate != Loaded) return;

		processor.placeAllConvsRandomly();
		processor.placeAllSpeakerRandomly();
	}
}

void Cs_plugInAudioProcessorEditor::changeListenerCallback(ChangeBroadcaster * source)
{/// gets called when a preset or saved state is loaded OR when the grain selection was changed via the x/y-plane
	if (source == &processor)
	{
		updateGUI2ChangedParams(processor.datastate);
	}
	else if (source == xy_plane)
	{
		if (xy_plane->dragCondition == 0)
		{
			valueTreeState.getParameter("curr_valence")->beginChangeGesture();
			valueTreeState.getParameter("curr_arousal")->beginChangeGesture();
		}
		else if (xy_plane->dragCondition == 1)
		{
			valueTreeState.getParameter("curr_valence")->setValueNotifyingHost(xy_plane->normX);
			valueTreeState.getParameter("curr_arousal")->setValueNotifyingHost(xy_plane->normY);
		}
		else if (xy_plane->dragCondition == 2)
		{
			valueTreeState.getParameter("curr_valence")->endChangeGesture();
			valueTreeState.getParameter("curr_arousal")->endChangeGesture();
		}
	}
	else if (source == deleteButton)
	{
		if (deleteButton->typeToDelete == DraggedElementType::ConversationIcon && deleteButton->convToDelete > -1)
		{
			processor.removeConversation(deleteButton->convToDelete);
		}
		else if (deleteButton->typeToDelete == DraggedElementType::ConvParticipantIcon && deleteButton->voiceToDelete > -1 && deleteButton->convToDelete > -1)
		{
			processor.remove_from_this_conv.add(deleteButton->convToDelete);
			processor.voices_to_remove.add(deleteButton->voiceToDelete);
		}
		else if (deleteButton->typeToDelete == DraggedElementType::SingleSpeakerIcon && deleteButton->speakerToDelete > -1)
		{
			processor.speakers_to_remove.add(deleteButton->speakerToDelete);
		}
		
	}
	else if (source == convPool)
	{
		
		if (convPool->broadcastThis == BroadcastValue::NewVoice && convPool->voiceToAdd > -1)
		{
			processor.add_to_this_conv.add(convPool->convToAddVoiceTo);
			processor.voices_to_add.add(convPool->voiceToAdd);
		}
		else if (convPool->broadcastThis == BroadcastValue::NewVoice && convPool->voiceToAdd == -1)
		{
			processor.add_random_to_conv.add(convPool->convToAddVoiceTo);
		}
		else if (convPool->broadcastThis == BroadcastValue::NewChattiness && convPool->newChattiness > -1 && convPool->convWithNewChattiness > -1)
		{
			processor.setChattiness(convPool->convWithNewChattiness, convPool->newChattiness);
		}
		else if (convPool->broadcastThis == BroadcastValue::Selection && convPool->selectedConv > -1)
		{
			processor.sel_conv.set(convPool->selectedConv);
			processor.sel_speaker.set(-1);
			processor.sel_voice.set(-1);
			updateSpeakerPanel();
			updateRoutingPanel();
		}
		
	}
	else if (source == singleSpeakerPanel)
	{
		if (singleSpeakerPanel->speakerToAdd > -1)
		{
			processor.speakers_to_add.add(singleSpeakerPanel->speakerToAdd);
		}
		if (singleSpeakerPanel->newSelection)
		{
			processor.sel_conv.set(-1);
			processor.sel_speaker.set(singleSpeakerPanel->selectedIcon);
			processor.sel_voice.set(-1);
			updateConvPool();
			updateRoutingPanel();
		}
	}

}

void Cs_plugInAudioProcessorEditor::updateGUI2ChangedParams(DataState datastate)
{/// called by change listener callback (-> by processor) to update GUI elements
	if (datastate == NotRequested)
	{
		openB->setButtonText("Load Data");
		openB->setEnabled(true);
		
		if (processor.guiUDList.contains(XYSlid) || create_new)
		{
			xy_plane->setSliderPos(*valueTreeState.getRawParameterValue("curr_valence"), *valueTreeState.getRawParameterValue("curr_arousal"));
			processor.guiUDList.removeAllInstancesOf(XYSlid);
		}
	}

	if (datastate == Loading)
	{
		openB->setEnabled(false);
		openB->setButtonText("Loading ...");
	}

	if (datastate == Loaded)
	{
		
		openB->setEnabled(true);
		openB->setButtonText("Load other");


		if (processor.guiUDList.contains(XYSlid) || create_new)
		{
			xy_plane->spotsX = processor.xValToDisp;
			xy_plane->spotsY = processor.yValtoDisp;
			xy_plane->spotsVoice = processor.voiceValtoDisp;
			xy_plane->isDataLoaded = true;
			xy_plane->setSliderPos(*valueTreeState.getRawParameterValue("curr_valence"), *valueTreeState.getRawParameterValue("curr_arousal"));
			
			xy_plane->repaint();

			processor.guiUDList.removeAllInstancesOf(XYSlid);
		}

		if (processor.guiUDList.contains(ConvSetup))
		{
			voiceSelectViewport->setViewedComponent(voiceSelectPalette = new VoiceSelectPalette(processor.syllables.size(), colors, 30));
			updateConvPool();
			processor.guiUDList.removeAllInstancesOf(ConvSetup);
		}

		if (processor.guiUDList.contains(SpeakPanel))
		{
			updateSpeakerPanel();
			processor.guiUDList.removeAllInstancesOf(SpeakPanel);
		}

		if (processor.guiUDList.contains(RoutingPanel))
		{
			updateRoutingPanel();
			processor.guiUDList.removeAllInstancesOf(RoutingPanel);
		}

		create_new = false;
	}
}

//void Cs_plugInAudioProcessorEditor::updateSpeakerSelectState()
//{/// called when selected speaker was changed
//	if (processor.datastate != Loaded) return;
//
//}
//
//void Cs_plugInAudioProcessorEditor::updateConvSelectState()
//{/// sets the state of converation gui components depending on the processor's conversations-vector
//	
//	
//}
//
//void Cs_plugInAudioProcessorEditor::updateTextFields()
//{/// loads the current routing values into the single spealers' text fields
//	if (processor.datastate == Loaded == 0) return;
//
//	
//}
//
//void Cs_plugInAudioProcessorEditor::updateConvTextFields()
//{/// loads the current routing values into the conversation routing text fields
//	if (processor.datastate == Loaded == 0) return;
//
//
//}

void Cs_plugInAudioProcessorEditor::setBoundsConvSetup()
{/// organizes layout of subcomponents
	convSetupGroup->setBounds(320, 10, 190, 310);
	voiceSelectViewport->setBounds(10, 15, 170, 70);
	voiceSelectPalette->setBounds(0, 0, 165, voiceSelectPalette->returnHeight());
	addConvButton->setBounds(10, 90, 80, 30);
	deleteButton->setBounds(100, 90, 80, 30);
	convPoolViewport->setBounds(10, 130, 170, 200);
}

void Cs_plugInAudioProcessorEditor::updateConvPool()
{/// rebuilds conversation pool with current conversation data from processor
	std::vector<Array<int>> voices;

	convPoolViewport->setViewedComponent(convPool = new ConversationPool(processor.conversations, colors, 25));
	convPool->addChangeListener(this);
	voiceSelectPalette->setBounds(0, 0, 165, voiceSelectPalette->returnHeight());
	convPool->setBounds(5, 130, 180, convPool->getLength());
}

void Cs_plugInAudioProcessorEditor::updateSpeakerPanel()
{/// rebuilds speaker panel with current speaker vector from processor
	Array<int> speakerVoices;

	for (int i = 0; i < processor.speakers.size(); ++i)
	{
		speakerVoices.add(processor.speakers[i].getVoice());
	}
	singleSpeakerViewport->setViewedComponent(singleSpeakerPanel = new SpeakerIconLine(colors, speakerVoices, 30, 1, 4));
	singleSpeakerPanel->addChangeListener(this);
	singleSpeakerPanel->setBounds(0, 0, 50, singleSpeakerPanel->getLength());
}

void Cs_plugInAudioProcessorEditor::updateRoutingPanel()
{///rebuilds routing panel

	int sel_conv = processor.sel_conv.get();
	int sel_speaker = processor.sel_speaker.get();

	if (sel_conv > -1 && sel_conv < processor.conversations.size())
	{
		routingViewport->setViewedComponent(routingPanel = new KnobRoutingLine(*this, processor.conversations[sel_conv].channels, processor.conversations[sel_conv].levels, processor.getTotalNumOutputChannels(), 35, 4));
		routingPanel->setBounds(0, 0, 83, routingPanel->getLength());
	}
	else if(sel_speaker > -1 && sel_speaker < processor.speakers.size())
	{
		routingViewport->setViewedComponent(routingPanel = new KnobRoutingLine(*this, processor.speakers[sel_speaker].channels, processor.speakers[sel_speaker].levels, processor.getTotalNumOutputChannels(), 35, 4));
		routingPanel->setBounds(0, 0, 83, routingPanel->getLength());
		
		if (routingPanel->knobs.size() > 0)
		{
			Rectangle<int> test = routingPanel->knobs[0]->getBounds();
		}
	}
}

