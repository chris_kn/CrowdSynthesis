/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.0.2

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "ui.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
ui::ui ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    addAndMakeVisible (voiceselect_slider = new Slider ("voiceselect_slider"));
    voiceselect_slider->setRange (0, 10, 0);
    voiceselect_slider->setSliderStyle (Slider::IncDecButtons);
    voiceselect_slider->setTextBoxStyle (Slider::TextBoxLeft, true, 80, 20);
    voiceselect_slider->addListener (this);

    addAndMakeVisible (numberspeaker_slider = new Slider ("numberspeaker_slider"));
    numberspeaker_slider->setRange (0, 20, 0);
    numberspeaker_slider->setSliderStyle (Slider::LinearBarVertical);
    numberspeaker_slider->setTextBoxStyle (Slider::TextBoxLeft, false, 80, 20);
    numberspeaker_slider->addListener (this);

    addAndMakeVisible (gain_slider = new Slider ("gain_slider"));
    gain_slider->setRange (0, 10, 0);
    gain_slider->setSliderStyle (Slider::Rotary);
    gain_slider->setTextBoxStyle (Slider::TextBoxLeft, false, 80, 20);
    gain_slider->addListener (this);

    addAndMakeVisible (xy_plane = new Component());

    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

ui::~ui()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    voiceselect_slider = nullptr;
    numberspeaker_slider = nullptr;
    gain_slider = nullptr;
    xy_plane = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}


//==============================================================================
void ui::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff323e44));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void ui::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    voiceselect_slider->setBounds (16, 16, 216, 24);
    numberspeaker_slider->setBounds (24, 48, 23, 160);
    gain_slider->setBounds (64, 48, 151, 168);
    xy_plane->setBounds (240, 16, 250, 250);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void ui::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == voiceselect_slider)
    {
        //[UserSliderCode_voiceselect_slider] -- add your slider handling code here..
        //[/UserSliderCode_voiceselect_slider]
    }
    else if (sliderThatWasMoved == numberspeaker_slider)
    {
        //[UserSliderCode_numberspeaker_slider] -- add your slider handling code here..
        //[/UserSliderCode_numberspeaker_slider]
    }
    else if (sliderThatWasMoved == gain_slider)
    {
        //[UserSliderCode_gain_slider] -- add your slider handling code here..
        //[/UserSliderCode_gain_slider]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="ui" componentName="" parentClasses="public Component"
                 constructorParams="" variableInitialisers="" snapPixels="8" snapActive="1"
                 snapShown="1" overlayOpacity="0.330" fixedSize="0" initialWidth="600"
                 initialHeight="400">
  <BACKGROUND backgroundColour="ff323e44"/>
  <SLIDER name="voiceselect_slider" id="62502064559f253a" memberName="voiceselect_slider"
          virtualName="" explicitFocusOrder="0" pos="16 16 216 24" min="0"
          max="10" int="0" style="IncDecButtons" textBoxPos="TextBoxLeft"
          textBoxEditable="0" textBoxWidth="80" textBoxHeight="20" skewFactor="1"
          needsCallback="1"/>
  <SLIDER name="numberspeaker_slider" id="ab5aeaed4efef854" memberName="numberspeaker_slider"
          virtualName="" explicitFocusOrder="0" pos="24 48 23 160" min="0"
          max="20" int="0" style="LinearBarVertical" textBoxPos="TextBoxLeft"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"
          needsCallback="1"/>
  <SLIDER name="gain_slider" id="769669a0feeea8d0" memberName="gain_slider"
          virtualName="" explicitFocusOrder="0" pos="64 48 151 168" min="0"
          max="10" int="0" style="Rotary" textBoxPos="TextBoxLeft" textBoxEditable="1"
          textBoxWidth="80" textBoxHeight="20" skewFactor="1" needsCallback="1"/>
  <JUCERCOMP name="xy_plane" id="3b8942640fa67c3c" memberName="xy_plane" virtualName=""
             explicitFocusOrder="0" pos="240 16 250 250" sourceFile="" constructorParams=""/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
