/*
  ==============================================================================

    CSLookAndFeel.h
    Created: 27 Jun 2017 10:45:31pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
#include<math.h>

class CSLookAndFeel : public LookAndFeel_V4
	/// class that adapts some graphical components
{
public:
	CSLookAndFeel()
	{
		
		setColour(GroupComponent::outlineColourId, Colours::grey);
		setColour(GroupComponent::textColourId, Colours::lightgrey);
		setColour(ScrollBar::trackColourId, Colours::black);
		setColour(ScrollBar::thumbColourId, Colours::darkgrey);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos, const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override
	{
		const float radius = jmin(width / 2, height / 2) - 5.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);
		Path p;
		const float pointerLength = radius * 1.f;
		const float pointerThickness = 1.f;
		p.addRectangle(-pointerThickness * 0.5f, -radius, pointerThickness, pointerLength);
		p.applyTransform(AffineTransform::rotation(angle).translated(centreX, centreY));
		
		Path q;
		float arc_radius = radius + 4.f;
		
		q.addArc(centreX - arc_radius, centreY - arc_radius, 2.f * arc_radius, 2.f * arc_radius, rotaryStartAngle, angle, true);
		
		// outline
		g.setColour(Colours::lightgrey);
		g.drawEllipse(rx, ry, rw, rw, 1.f);

		// pointer
		g.setColour(Colours::lightgrey);
		g.fillPath(p);

		// highlight
		g.setColour(Colours::aquamarine);
		
		//g.strokePath(q, PathStrokeType(2.f));
		g.strokePath(q, PathStrokeType(2.f, PathStrokeType::JointStyle::curved, PathStrokeType::EndCapStyle::rounded));
	}

	void drawGroupComponentOutline(Graphics & g, int w, int h, const String &text, const Justification & justi, GroupComponent &) override
	{
		Path r;
		String uptext = text.toUpperCase();
		const float headroom = 5.5f;
		float titleroom;
		if (text.isNotEmpty())
		{
			titleroom = 5.f;
		}
		
		else
		{
			titleroom = 0.f;
		}
		
		const float height = (float)h;
		const float width = (float)w;
		const float cornersize = 7.f;
		const float lineThickness = .5f;
		const float title_pos = .1f * width;
		const float str_width = g.getCurrentFont().getStringWidthFloat(uptext);
		
		g.setColour(Colours::white);
		
		g.drawText(uptext, Rectangle<float>(title_pos + titleroom, 0, width - 2.f*title_pos - 2.f*titleroom, 2.f * headroom), justi);
		g.excludeClipRegion(Rectangle<int>((int)title_pos, 0, str_width + (int) 2.f * titleroom, (int)headroom * 2.f));
		g.drawRoundedRectangle(Rectangle<float>(0.5f, headroom, width-1.f, height - headroom -.5f), cornersize, lineThickness);
		
	}

	void drawLinearSlider(Graphics & g, int xi, int yi, int widthi, int heighti, float sliderPosi, float minSliderPos, float maxSliderPos, const Slider::SliderStyle, Slider& slider) override
	{
		
		//g.fillAll(Colours::red);
		Rectangle<int> originalClipRegion(g.getClipBounds());

		int x = 0;
		int y = 0;
		int width = originalClipRegion.getWidth();
		int height = originalClipRegion.getHeight();
		float sliderPos = (1.f - slider.getValue()) * height;
		
		if (!slider.isMouseOver())
		{
			g.fillAll(Colours::black.withAlpha(.1f));
		}

		g.setColour(Colours::aquamarine);
		g.fillRect(Rectangle<float>(0.5f, sliderPos, width - 1.0f, height - sliderPos));

		g.setColour(Colours::grey);
		g.drawRect(x + 0.5f, y + 0.5f, width - 1.0f, height - 1.0f);

		g.setColour(Colours::white);

		float centerX = x + width / 2.f;
		float centerY = y + height / 2.f;
		Rectangle<float> exPeak(centerX - .2f*width, centerY, .15f*width, .4f * height);
		Rectangle<float> bubble(centerX - .3f * width, centerY - .2f * height, .6f * width, .35f * height);
		//Rectangle<int> overlapRec = bubble.getIntersection(exPeak).toNearestInt();
		
		Path arrow;
		arrow.startNewSubPath(exPeak.getCentreX() - exPeak.getWidth() / 2.f, bubble.getCentreY() + bubble.getHeight() / 2.f);
		arrow.lineTo(exPeak.getCentreX() - exPeak.getWidth() / 2.f, y + .7f * height);
		arrow.lineTo(exPeak.getCentreX() + exPeak.getWidth() / 2.f, bubble.getCentreY() + bubble.getHeight() / 2.f);
		
		g.strokePath(arrow, PathStrokeType(1.5f));
		
		g.excludeClipRegion(exPeak.toNearestInt());
		g.drawRoundedRectangle(bubble, 3.f, 1.f);
		
	}

};


