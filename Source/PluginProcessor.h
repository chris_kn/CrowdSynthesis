/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SingleSpeaker.h"
#include "Syllable.h"
#include "NewConversation.h"

enum DataState {
	NotRequested,
	Loading,
	Loaded
};

enum GUIUpdateThis {
	XYSlid,
	SpeakPanel,
	RoutingPanel,
	ConvSetup
};

//==============================================================================
/**
*/
class Cs_plugInAudioProcessor  : public AudioProcessor,
								 public Thread,
								 public ChangeBroadcaster,
								 public AudioProcessorValueTreeState::Listener,
								// private OSCReceiver,
	private OSCReceiver::Listener<OSCReceiver::MessageLoopCallback>
	/// The heart of the plugin. Contains the functions called by the host (especially the audio thread) and to load the audio data. Creates the needed speaker and converstaion objects and an instance of the plugin editor (= the gui). 
{
public:

    //==============================================================================
    Cs_plugInAudioProcessor();
    ~Cs_plugInAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

	bool setPreferredBusArrangement(bool isInputBus, int busIndex, const AudioChannelSet & preferredLayout);

	void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

	void processSpeaker(AudioSampleBuffer & buffer, SingleSpeaker & curr_speaker);

	void processConversation(AudioSampleBuffer & buffer, NewConversation & conv);

	template<typename T> void processStream(AudioSampleBuffer & buffer, T & curr_speaker);

	void handleMIDIEvents(MidiBuffer & midiMessages);

	void checkForUpdatedParams(AudioSampleBuffer& buffer);

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

	bool canAddBus(bool is_input) const override;

	bool canRemoveBus(bool is_input) const override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

	void initiateLoading();
	
	void addRandomChannel(int s);
	void placeOneSpeakerRandomly();
	void placeAllSpeakerRandomly();

	void placeConvRandomly();
	void placeAllConvsRandomly();

	void addConversation();
	void addConversations(int howMany);
	void removeConversation(int num_conv);

	void setChattiness(int convIndex, float value);
	float getChattiness(int convIndex);

	void updateAverageRMS(float arousal, bool normed);

	void showConnectionErrorMessage(const String & messageText);

	// PARAMETERS & INTERNAL DATA ======
	// vectors of single speakers and conversations -> audio streams
	std::vector<SingleSpeaker> speakers;
	std::vector<NewConversation> conversations;
	std::vector<std::vector<Syllable>> syllables;
	DataState datastate;
	Array<GUIUpdateThis> guiUDList;

	// x/y-plane values for drawing spots
	std::vector<float> xValToDisp;
	std::vector<float> yValtoDisp;
	std::vector<int> voiceValtoDisp;

	// "bool" atomic values to check for updates from all different components
	// "1" -> true, "0" -> false !!!
	Atomic<int> mustRefreshSelGrainMatrix;
	Atomic<int> mustLoadNewSpeakerArray;
	Atomic<int> mustUpdateSpeaker;
	Atomic<int> areThereNewPanningValues;
	Atomic<int> areThereNewConvPanningValues;
	Atomic<int> mustUpdateConversation;
	Atomic<int> mustLoadNewConversationArray;
	Atomic<int> is_rand_voice_activated;

	// single speaker parameters
	Array<int> speakers_to_add;
	Array<int> speakers_to_remove;
	Array<int> random_speaker_to_add;
	Atomic<int> newPanningSpeaker;
	Atomic<int> newPanningChannel;
	Atomic<float> newPanningLevel;
	Atomic<int> sel_voice;
	Atomic<int> sel_speaker;

	// conversation parameters
	Array<int> voices_to_add;
	Array<int> add_to_this_conv;
	Array<int> add_random_to_conv;
	Array<int> voices_to_remove;
	Array<int> remove_from_this_conv;
	Atomic<int> sel_conv;
	Atomic<int> sel_conv_voice;
	int newConvPanningChannel;
	float newConvPanningLevel;

	const float default_level;
	int target_outputChannels;

private:

	ScopedPointer<OSCReceiver> oscReceiver;
	AudioProcessorValueTreeState parameters;
	float previousGain;
	File source_folder;

	float average_rms;

	std::vector<SingleSpeaker> newSpeakers;
	std::vector<NewConversation> newConversations;
	
	// single speaker random channel assign
	SingleSpeaker updatedSpeaker;
	int voiceNumbUpdatedSpeaker;
	int speakerNumbUpdatedSpeaker;

	// conversation random channel assign
	NewConversation updatedConversation;
	int convNumbUpdatedConversation;

	int defaultOutputs;

	int crossfadeLength;
	int minUnstressedTime;
	const float default_notPausePropability;
	
	AudioFormatManager formatManager;
	std::vector<Array<int>> selectedGrains;
	std::vector<Array<int>> temp_selectedGrains;

	AudioBuffer<float> copybuffer1;
	AudioBuffer<float> copybuffer2;

	double fs;

	std::vector<float> upper_rms_limit;

	Random random;
	int key26Limit;

	void run() override;
	void loadDataFolders(File directory);
	std::vector<Syllable> loadDataVector(File directory, int voicenumber);
	void fillNextGrainBuffers(SingleSpeaker& speaker);
	void fillNextGrainBuffers(NewConversation & conv);
	template <typename T> void fillNextGrainBuffersTemplate(T & speaker);
	void buildSelectedGrainMatrix(std::vector<std::vector<Syllable>>&, float, float, float, float);
	float rangeToMin(float middle, float range);
	float rangeToMax(float middle, float range);

	void updatePanningValues(int speaker, int channel, float level);
	void updateConvPanningValues(int num_conv, int channel, float level);

	void analyseCorpus(std::vector<std::vector<Syllable>>&);
	void parameterChanged(const String &parameterID, float newValue) override;
	void oscMessageReceived(const OSCMessage& message) override;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Cs_plugInAudioProcessor)
};
