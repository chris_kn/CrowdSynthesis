/*
==============================================================================

Speaker.h
Created: 9 Jan 2017 3:31:39pm
Author:  C. Knörzer

==============================================================================
*/

//#ifndef SPEAKER_H_INCLUDED
//#define SPEAKER_H_INCLUDED
#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
#include <ctime>
#include "Ringbuffer.h"

#define NOTPAUSE_PROBABILITY 0.95f

enum class StreamState {
	///stores the state of the conversation or speaker, whether it is currently playing one syllable or fading etc.
	PlaySyll = 0,
	Crossfade,
	Fadeout,
	Pausing,
	Inactive,
	NoGrainsInBuffer
};

enum Pitch {
	/// stores the pitch of the speaker or voice within a conversation
	original = 0,
	upwards,
	downwards
};

class TalkingStream
	/// base class for a stream of glibberish talk. Can't be used like this, there are virtual methods who need to be implemented to actually create the streams. Done so in "SingleSpeaker.h" and "NewConversation.h". It holds a ringbuffer with the indexes of the next grains to be copied to the channel output stream(s).
{
public:
	TalkingStream()
		: position(0),
		shallSpeak(false),
		speakerState(StreamState::Pausing),
		pauseLength(5000),
		newSyllable(-1),
		fadePosition(0),
		old_RMS_factor(1.f),
		new_RMS_factor(1.f)
	{
		random.setSeedRandomly();
		channels.add(0);
		levels.add(.9f);
	}

	TalkingStream(float default_level)
		: position(0),
		shallSpeak(false),
		speakerState(StreamState::Pausing),
		pauseLength(5000),
		newSyllable(-1),
		fadePosition(0),
		old_RMS_factor(1.f),
		new_RMS_factor(1.f)
	{
		random.setSeedRandomly();
		channels.add(0);
		levels.add(default_level);
	}

	TalkingStream(Array<int> newChannels, Array<float> newLevels)
		: position(0),
		shallSpeak(false),
		speakerState(StreamState::Pausing),
		pauseLength(5000),
		newSyllable(-1),
		fadePosition(0),
		channels(newChannels),
		levels(newLevels),
		old_RMS_factor(1.f),
		new_RMS_factor(1.f)
	{
		random.setSeedRandomly();
	}

	~TalkingStream() {
		channels.clear();
		levels.clear();
	};

	// ========== PUBLIC PARAMETERs =======
	bool shallSpeak;

	int position;

	int oldSyllable;
	int newSyllable;

	int fadePosition;
	int pauseLength;

	float old_RMS_factor;
	float new_RMS_factor;

	Random random;

	StreamState speakerState;
	Array<int> channels;
	Array<float> levels;

	//============ VIRTUAL METHODS ================
	virtual bool isReadyToPlay() = 0;

	virtual int getVoice() = 0;

	virtual int getPitch() = 0;

	/// has to be implemented to update certain values to transfer from one state to the other
	virtual void prepareNewState(StreamState oldState, int crossFadeLength, double fs) = 0;

	virtual void generatePause(int fs) = 0;

	//========= PUBLIC METHODS ====================

	void setActive()
	{/// can be triggered e.g. by midi noteon events to tell the object it should talk
		shallSpeak = true;
	}

	
	void setInactive()
	{/// can be triggered e.g. by midi noteoff events to tell the object to stop talking
		shallSpeak = false;
	}

	
	void set_min_grains_before_pause(int number)
	{/// sets the number of grains that have to be spoken before a pause can be triggered
		min_grains_before_pause = number;
	}

	int get_min_grains_before_pause()
	{
		return min_grains_before_pause;
	}

	void setLevel(int channel, float level)
	{/// set the level for one channel output (levels over 1 are set to 1, if the level is less than 0.1f, the channel is removed from the object's channel list)
		
		if (level < .1f)
		{
			int index = channels.indexOf(channel);
			channels.remove(index);
			levels.remove(index);
		}
		else if (level > 1.0f)
		{
			channels.addIfNotAlreadyThere(channel);
			int index = channels.indexOf(channel);
			levels.resize(channels.size());
			if (index >= 0 && index < levels.size())
			{
				levels.set(index, 1.0f);
			}
		}
		else
		{
			channels.addIfNotAlreadyThere(channel);
			int index = channels.indexOf(channel);
			levels.resize(channels.size());
			if (index >= 0 && index < levels.size())
			{
				levels.set(index, level);
			}
		}
	}

	float getLevel(int channel)
	{/// get the level for one channel, if the channel is not on the object's channel list, it returns 0

		int index = channels.indexOf(channel);
		if (index >= 0 && index < levels.size())
		{
			return levels[index];
		}
		else
		{
			return 0.f;
		}
	}

	
	void setPanning(Array<int> newChannels, Array<float> newLevels)
	{/// exchange the whole channel and level lists/arrays
		channels = newChannels;
		levels = newLevels;
	}

	void placeRandomly(int maxChannel, float level)
	{/// the object is placed on one random channel between (including) 0 and (excluding) maxChannel, all other channels are deleted! maxChannel should be the number of channels available, not the index of the last channel!
		int newChannel = random.nextInt(maxChannel);
		channels.clear();
		levels.clear();
		channels.add(newChannel);
		levels.add(level);
	}

	void addRandomChannel(int maxChannel, float level)
	{
		Array<int> possible;
		for (int i = 0; i < maxChannel; ++i)
		{
			if (!channels.contains(i)) possible.add(i);
		}
		int newChannel = random.nextInt(possible.size());
		
		channels.add(possible[newChannel]);

		if (level <= 1 && level >= 0)
		{
			levels.add(level);
		}
		else
		{
			levels.add(0.5);
		}
	}

	bool hasChannel(int channel)
	{/// check if channel is on object's channel list
		return channels.contains(channel);
	}

	void resetRMS()
	{/// sets rms-factors to 1
		old_RMS_factor = 1.f;
		new_RMS_factor = 1.f;
	}

private:
	int min_grains_before_pause;

};




//#endif

