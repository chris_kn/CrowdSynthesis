/*
  ==============================================================================

    KnobRoutingLine.h
    Created: 25 Oct 2017 1:58:35pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class KnobRoutingLine    : public Component
{/// GUI component: contains the routing knobs to set the output channels of the selected item
public:
    KnobRoutingLine(SliderListener& editor, Array<int> channels, Array<float> levels, int numChannels, int pxsize = 40, int margin = 3)
		: margin(margin),
		pxsize(pxsize)
    {
		
		for (int i = 0; i < numChannels; ++i)
		{
			knobs.add(new Slider());
			knobs[i]->setRange(0.0, 1.0, .01);
			
			knobs[i]->setSliderStyle(Slider::SliderStyle::Rotary);
			knobs[i]->setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxRight, false, 40, 20);
			if (channels.contains(i) && levels.size() > channels.indexOf(i))
			{
				knobs[i]->setValue(levels[channels.indexOf(i)], dontSendNotification);
			}
			addAndMakeVisible(knobs[i]);
			knobs[i]->addListener(&editor);
		}

    }

    ~KnobRoutingLine()
    {
    }

    void paint (Graphics& g) override
    {
        g.setColour (Colours::grey);
        g.drawRect (getLocalBounds(), 1);   // draw an outline around the component

    }

    void resized() override
    {/// organizes the layout of the knobs

		for (int i = 0; i < knobs.size(); ++i)
		{
			knobs[i]->setBounds(0, margin + (pxsize + margin) * i, getWidth()-4, pxsize);
		}

    }

	int getLength()
	{/// returns an estimate of the pixels needed to display the knobs
		int contentLength = 4 * margin + (margin + pxsize) * knobs.size();
		return std::max(contentLength, pxsize);
	}

	OwnedArray<Slider> knobs;
	int pxsize;
	int margin;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (KnobRoutingLine)
};
