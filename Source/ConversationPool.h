/*
  ==============================================================================

    ConversationPool.h
    Created: 23 Oct 2017 8:00:43pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ConversationPad.h"
#include "NewConversation.h"

enum BroadcastValue {
	/// defines, which information is new to be read by change listener
	NewVoice = 0,
	NewChattiness,
	Selection
};

//==============================================================================
/*
*/
class ConversationPool : public Component, public ChangeListener, public ChangeBroadcaster, public SliderListener
{/// GUI component containing all the conversation pads
public:

	ConversationPool(std::vector<NewConversation> convdata, Array<Colour> colors, int pxsize, int selectedConv = -1)
		:convPads(),
		voiceToAdd(-1),
		convToAddVoiceTo(-1),
		newChattiness(-1),
		convWithNewChattiness(-1),
		selectedConv(selectedConv)
	{
		for (int i = 0; i < convdata.size(); ++i)
		{
			convPads.add(new ConversationPad(colors, convdata[i].voices, pxsize, i, convdata[i].chattiness.get()));
			convPads[i]->addChangeListener(this);
			convPads[i]->chattinessSlider->addListener(this);
			convPads[i]->addMouseListener(this, false);
			addAndMakeVisible(convPads[i]);
		}
	}

    ~ConversationPool()
    {
		for (int i = 0; i < convPads.size(); ++i)
		{
			convPads[i]->removeMouseListener(this);
		}
    }

    void paint (Graphics& g) override
    {

    }

    void resized() override
    {
        /// organizes the layout of the subcomponents (the conversation pads)

		for (int i = 0; i < convPads.size(); ++i)
		{
			convPads[i]->setBounds(0, i * 45, 170, 40);
		}

    }

	int getSize()
	{
		return convPads.size();
	}

	int getLength()
	{
		return convPads.size() * 45 + 2;
	}

	void changeListenerCallback(ChangeBroadcaster * source)
	{/// called when one of the conversation pads broadcasts a change (when voice was added). Forwards the change to the plugin editor.
		for (int c = 0; c < convPads.size(); ++c)
		{
			if (source == convPads[c])
			{
				voiceToAdd = convPads[c]->voiceToAdd;
				convToAddVoiceTo = c;
				broadcastThis = BroadcastValue::NewVoice;
				sendChangeMessage();
			}
		}
	}

	void sliderValueChanged(Slider * sliderMoved)
	{/// called when one of the conversation pads' chattiness sliders was moved
		for (int c = 0; c < convPads.size(); ++c)
		{
			if (sliderMoved == convPads[c]->chattinessSlider)
			{
				convWithNewChattiness = c;
				newChattiness = sliderMoved->getValue();
				broadcastThis = BroadcastValue::NewChattiness;
				sendChangeMessage();
			}
		}
	}

	void mouseUp(const MouseEvent& e)
	{/// checks if click on one of the conv pads is a new selection. If yes, it is passed to the plugin editor.
		bool newSelection = false;

		for (int i = 0; i < convPads.size(); ++i)
		{
			if (e.eventComponent == convPads[i] && i != selectedConv)
			{
				selectedConv = i;
				newSelection = true;
			}
		}

		if (newSelection)
		{
			for (int i = 0; i < convPads.size(); ++i)
			{
				convPads[i]->selected = false;
				convPads[i]->repaint();
			}

			convPads[selectedConv]->selected = true;
			convPads[selectedConv]->repaint();

			broadcastThis = BroadcastValue::Selection;
			sendChangeMessage();
		}
	}

	// Parameters
	float newChattiness;
	int convWithNewChattiness;
	int voiceToAdd;
	int convToAddVoiceTo;
	int selectedConv;
	BroadcastValue broadcastThis;
	OwnedArray<ConversationPad> convPads;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConversationPool)
};
