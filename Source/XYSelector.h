/*
  ==============================================================================

    XYSelector.h
    Created: 8 Jun 2017 11:04:01pm
    Author:  C. Knörzer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class XYSelector    : public Component
/// the rectangle element within a XYSlider to select a two-dimensional range, the "handle" of the slider
{
public:
    XYSelector(int sizeCenter = 6)
    {
		this->sizeCenter = sizeCenter;
		borders = new ComponentBoundsConstrainer();
		borders->setMinimumOnscreenAmounts(0xffffff, 0xffffff, 0xffffff, 0xffffff);
    }

    ~XYSelector()
    {
    }

    void paint (Graphics& g) override
    {/// draws the center and the outlines

        g.setColour (Colours::grey);
        g.drawRect (getLocalBounds(), 2);   // draw an outline around the component

        g.setColour (Colours::white);
		g.fillRect(getXMiddle() - floor(sizeCenter / 2), getYMiddle() - floor(sizeCenter / 2), sizeCenter, sizeCenter);
        
    }

    void resized() override
    {
    }

	int getXMiddle()
	{/// returns the horizontal center in pixels
		return floor(this->getWidth() / 2);
	}

	int getYMiddle()
	{/// returns the vertical center in pixels
		return floor(this->getHeight() / 2);
	}

	ScopedPointer<ComponentBoundsConstrainer> borders;
	int sizeCenter;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (XYSelector)
};
