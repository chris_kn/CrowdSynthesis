var class_single_speaker =
[
    [ "SingleSpeaker", "class_single_speaker.html#a9bf8b5f07a3747ac0129ed08f96792f1", null ],
    [ "SingleSpeaker", "class_single_speaker.html#aaff2a690a1f030803a93861293b5a927", null ],
    [ "SingleSpeaker", "class_single_speaker.html#a419f6958c64d260d2e563eb54d5ec1fb", null ],
    [ "generatePause", "class_single_speaker.html#a1107fb1b8cc0a500e4849de0d8b52185", null ],
    [ "getPitch", "class_single_speaker.html#a4987a58d197fbaec9e0ef4708c4acdf7", null ],
    [ "getVoice", "class_single_speaker.html#ad4097a2702abf5908f2ec2ff39628880", null ],
    [ "prepareNewState", "class_single_speaker.html#ac070df1aed638e192d6cccd09dd84812", null ],
    [ "pitch", "class_single_speaker.html#ab935255d87a3b30b02fd8e1aedcfc745", null ],
    [ "voice", "class_single_speaker.html#ac1ec0f78fc31dba4f053c76edee9da7d", null ]
];