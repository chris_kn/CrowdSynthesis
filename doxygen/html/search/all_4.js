var searchData=
[
  ['generatepause',['generatePause',['../class_new_conversation.html#a2565e24b8c3c03c1593d6e6422791be8',1,'NewConversation']]],
  ['getbuffer',['getBuffer',['../class_single_speaker.html#a10e639eddc2187ad1eacfb66b955bdad',1,'SingleSpeaker']]],
  ['getchattiness',['getChattiness',['../class_cs__plug_in_audio_processor.html#a26578296543cc85b60ab2376e65f309d',1,'Cs_plugInAudioProcessor']]],
  ['getlastvoice',['getLastVoice',['../class_new_conversation.html#adc9006ecbba85e27a1cb23606f08e073',1,'NewConversation']]],
  ['getlength',['getLength',['../class_conv_voice_icon_line.html#a417bd42907ecd2a5f5fd98d786a4193d',1,'ConvVoiceIconLine::getLength()'],['../class_knob_routing_line.html#a8b5d47125f112aca90c1a52ccdcc2445',1,'KnobRoutingLine::getLength()']]],
  ['getlevel',['getLevel',['../class_talking_stream.html#a9954c5456523ea5e6c5fe74a749b5403',1,'TalkingStream']]],
  ['getnextsyllable',['getNextSyllable',['../class_new_conversation.html#ac593e1eb8d3444d5e3ee654a4469aa1e',1,'NewConversation']]],
  ['getnumvoices',['getNumVoices',['../class_conversation_pad.html#aa60e77e089fce5513c2109c6a350a576',1,'ConversationPad']]],
  ['getpitch',['getPitch',['../class_new_conversation.html#a36db19ba6c5f4849fd9b2beb3f1d41e7',1,'NewConversation::getPitch()'],['../class_single_speaker.html#a4987a58d197fbaec9e0ef4708c4acdf7',1,'SingleSpeaker::getPitch()']]],
  ['getstateinformation',['getStateInformation',['../class_cs__plug_in_audio_processor.html#afc49a2c8d729e8ca8d6deb4844206ff6',1,'Cs_plugInAudioProcessor']]],
  ['getvoice',['getVoice',['../class_new_conversation.html#a3539ad452f35e0aa213d7bd59cb60a9d',1,'NewConversation::getVoice()'],['../class_new_conversation.html#ab48511a5873678967b8bd0a27a340342',1,'NewConversation::getVoice(int i)'],['../class_single_speaker.html#ad4097a2702abf5908f2ec2ff39628880',1,'SingleSpeaker::getVoice()']]],
  ['getxmiddle',['getXMiddle',['../class_x_y_selector.html#a8e39abaa59193e79d488b24873b2a049',1,'XYSelector::getXMiddle()'],['../class_x_y_slider.html#a4b1169fcfd2a2706bf390cee28364495',1,'XYSlider::getXMiddle()']]],
  ['getymiddle',['getYMiddle',['../class_x_y_selector.html#acf4c28ee3b60b2f02934a94ff08832cc',1,'XYSelector::getYMiddle()'],['../class_x_y_slider.html#aaa197d12d052a791e723ae11f55dfaac',1,'XYSlider::getYMiddle()']]],
  ['grainbuffer',['GrainBuffer',['../class_grain_buffer.html',1,'GrainBuffer'],['../class_grain_buffer.html#a37bb959c43cc846d654fbf4cb521590e',1,'GrainBuffer::GrainBuffer()']]]
];
