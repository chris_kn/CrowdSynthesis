var class_x_y_slider =
[
    [ "XYSlider", "class_x_y_slider.html#a5dca533612787821cbc891100de603cc", null ],
    [ "~XYSlider", "class_x_y_slider.html#a4bfe05a864a16cd7ec47ae073e3ac6b4", null ],
    [ "getXMiddle", "class_x_y_slider.html#a4b1169fcfd2a2706bf390cee28364495", null ],
    [ "getYMiddle", "class_x_y_slider.html#aaa197d12d052a791e723ae11f55dfaac", null ],
    [ "paint", "class_x_y_slider.html#acfdee0aa403b17cf277e06c8acfa384d", null ],
    [ "resized", "class_x_y_slider.html#ae76ee574c5d806331687f616ff89e71a", null ],
    [ "setSliderPos", "class_x_y_slider.html#af70dc0f32f5a0895cc4370b45a8117dd", null ],
    [ "setXSpread", "class_x_y_slider.html#ac417de94f06561a88a5ac10c035458ee", null ],
    [ "setYSpread", "class_x_y_slider.html#a8ac1760e96d1075688cc16930aa5aa92", null ],
    [ "dragCondition", "class_x_y_slider.html#a7552aa6403b805abf6b04096900be001", null ],
    [ "isDataLoaded", "class_x_y_slider.html#a0f17212beb3fd40bc8ae469dcafd0d90", null ],
    [ "normX", "class_x_y_slider.html#a9ef4589bf2aac171024d439670ae3288", null ],
    [ "normY", "class_x_y_slider.html#a24e9b28fef9999d238c719d312a66b85", null ],
    [ "spotcolors", "class_x_y_slider.html#a2a458e162410ccabe308742d6a2f064d", null ],
    [ "spotsVoice", "class_x_y_slider.html#a1c3535ce0ab127fedd242e61f98c8210", null ],
    [ "spotsX", "class_x_y_slider.html#acce498d09c1549019100bd6841d300aa", null ],
    [ "spotsY", "class_x_y_slider.html#adb7b8308d6abdde8655505338fadc98d", null ],
    [ "voiceSlider_dragged", "class_x_y_slider.html#a88b7455b88892fbf08882e2147930e77", null ]
];