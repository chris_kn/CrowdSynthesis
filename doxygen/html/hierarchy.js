var hierarchy =
[
    [ "AudioProcessor", null, [
      [ "Cs_plugInAudioProcessor", "class_cs__plug_in_audio_processor.html", null ]
    ] ],
    [ "AudioProcessorEditor", null, [
      [ "Cs_plugInAudioProcessorEditor", "class_cs__plug_in_audio_processor_editor.html", null ]
    ] ],
    [ "ButtonListener", null, [
      [ "Cs_plugInAudioProcessorEditor", "class_cs__plug_in_audio_processor_editor.html", null ]
    ] ],
    [ "ChangeBroadcaster", null, [
      [ "Cs_plugInAudioProcessor", "class_cs__plug_in_audio_processor.html", null ],
      [ "XYSlider", "class_x_y_slider.html", null ]
    ] ],
    [ "ChangeListener", null, [
      [ "Cs_plugInAudioProcessorEditor", "class_cs__plug_in_audio_processor_editor.html", null ]
    ] ],
    [ "Component", null, [
      [ "SpeakerRoutingLine", "class_speaker_routing_line.html", null ],
      [ "ui", "classui.html", null ],
      [ "XYSelector", "class_x_y_selector.html", null ],
      [ "XYSlider", "class_x_y_slider.html", null ]
    ] ],
    [ "Listener", null, [
      [ "Cs_plugInAudioProcessorEditor", "class_cs__plug_in_audio_processor_editor.html", null ]
    ] ],
    [ "LookAndFeel_V4", null, [
      [ "CSLookAndFeel", "class_c_s_look_and_feel.html", null ]
    ] ],
    [ "Ringbuffer", "class_ringbuffer.html", null ],
    [ "SliderListener", null, [
      [ "Cs_plugInAudioProcessorEditor", "class_cs__plug_in_audio_processor_editor.html", null ],
      [ "ui", "classui.html", null ]
    ] ],
    [ "Syllable", "class_syllable.html", null ],
    [ "TalkingStream", "class_talking_stream.html", [
      [ "NewConversation", "class_new_conversation.html", null ],
      [ "SingleSpeaker", "class_single_speaker.html", null ]
    ] ],
    [ "Thread", null, [
      [ "Cs_plugInAudioProcessor", "class_cs__plug_in_audio_processor.html", null ],
      [ "Cs_plugInAudioProcessorEditor", "class_cs__plug_in_audio_processor_editor.html", null ]
    ] ]
];