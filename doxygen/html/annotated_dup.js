var annotated_dup =
[
    [ "Cs_plugInAudioProcessor", "class_cs__plug_in_audio_processor.html", "class_cs__plug_in_audio_processor" ],
    [ "Cs_plugInAudioProcessorEditor", "class_cs__plug_in_audio_processor_editor.html", "class_cs__plug_in_audio_processor_editor" ],
    [ "CSLookAndFeel", "class_c_s_look_and_feel.html", "class_c_s_look_and_feel" ],
    [ "NewConversation", "class_new_conversation.html", "class_new_conversation" ],
    [ "Ringbuffer", "class_ringbuffer.html", "class_ringbuffer" ],
    [ "SingleSpeaker", "class_single_speaker.html", "class_single_speaker" ],
    [ "SpeakerRoutingLine", "class_speaker_routing_line.html", "class_speaker_routing_line" ],
    [ "Syllable", "class_syllable.html", "class_syllable" ],
    [ "TalkingStream", "class_talking_stream.html", "class_talking_stream" ],
    [ "ui", "classui.html", "classui" ],
    [ "XYSelector", "class_x_y_selector.html", "class_x_y_selector" ],
    [ "XYSlider", "class_x_y_slider.html", "class_x_y_slider" ]
];